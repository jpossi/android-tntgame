/*******************************************************************************
 * Copyright 2014 Jascha Starke <jascha@ja-s.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.jaschastarke.android.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

public abstract class DBHelper extends SQLiteOpenHelper {
    protected static final String NO_NULLABLE_COLUMN = "null";
    protected static final String WHERE_ID_MATCH = BaseColumns._ID + " = ?";
    
    /*@TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public DBHelper(Context context, String name, CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }*/
    public DBHelper(Context context, String name, CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public long insert(DBEntry entry) {
        ContentValues values = entry.getContentValues();
        boolean setId = false;
        if (values.containsKey(BaseColumns._ID) && values.getAsLong(BaseColumns._ID) <= 0) {
            values.remove(BaseColumns._ID);
            setId = true;
        }
        long id = getWritableDatabase().insert(
            entry.getTable().getName(),
            NO_NULLABLE_COLUMN,
            values);
        if (entry instanceof AbstractStringEntry && setId)
            ((AbstractStringEntry) entry).setId(id);
        return id;
    }
    public long replace(DBEntry entry) {
        ContentValues values = entry.getContentValues();
        if (values.containsKey(BaseColumns._ID) && values.getAsLong(BaseColumns._ID) <= 0)
            throw new IllegalArgumentException("To use replace, a PK have to be defined");
        long id = getWritableDatabase().replace(
            entry.getTable().getName(),
            NO_NULLABLE_COLUMN,
            values);
        return id;
    }
    public int update(DBEntry entry) {
        return getWritableDatabase().update(
            entry.getTable().getName(),
            entry.getContentValues(),
            WHERE_ID_MATCH,
            new String[]{String.valueOf(entry.getId())});
    }
    public int delete(DBEntry entry) {
        return getWritableDatabase().delete(
            entry.getTable().getName(),
            WHERE_ID_MATCH,
            new String[]{String.valueOf(entry.getId())});
    }
    public DBEntry find(SimpleDBTable table, long id) {
        Cursor c = getWritableDatabase().query(
            table.getName(),
            table.getColumns(),
            WHERE_ID_MATCH,
            new String[]{String.valueOf(id)},
            null, // group
            null, // having
            null); // orderby
        DBEntry entry = null;
        if (c.moveToFirst())
            entry = table.createEntry(c);
        c.close();
        return entry;
    }
    public <T extends DBEntry> T find(DBTable<T> table, long id) {
        Cursor c = getWritableDatabase().query(
            table.getName(),
            table.getColumns(),
            WHERE_ID_MATCH,
            new String[]{String.valueOf(id)},
            null, // group
            null, // having
            null); // orderby
        return fetchOne(table, c);
    }
    protected <T extends DBEntry> T fetchOne(DBTable<T> table, Cursor c) {
        T entry = null;
        if (c.moveToFirst())
            entry = table.createEntry(c);
        c.close();
        return entry;
    }
}
