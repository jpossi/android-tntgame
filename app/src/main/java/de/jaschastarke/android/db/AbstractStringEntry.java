/*******************************************************************************
 * Copyright 2014 Jascha Starke <jascha@ja-s.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.jaschastarke.android.db;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import android.content.ContentValues;
import android.database.Cursor;
import android.provider.BaseColumns;

public abstract class AbstractStringEntry implements DBEntry, Serializable {
    private static final long serialVersionUID = -7725376614351660406L;
    
    private long id = 0;
    protected Map<String, String> data = new HashMap<String, String>();
    
    public AbstractStringEntry() {
    }
    public AbstractStringEntry(Cursor c) {
        fillFromCursor(c);
    }
    
    public void setId(long insertId) {
        id = insertId;
    }
    public long getId() {
        return id;
    }
    
    public String get(String column) {
        return data.get(column);
    }
    public String get(String column, String defaultValue) {
        String v = data.get(column);
        return v == null ? defaultValue : v;
    }
    /**
     * @returns null instead of an empty string, even if an empty string is stored in DB
     */
    public String getNotEmpty(String column) {
        String v = get(column);
        if (v != null && v.equals(""))
            return null;
        return v;
    }
    public void set(String column, String value) {
        data.put(column, value);
    }
    
    protected void setBool(String column, boolean value) {
        data.put(column, value ? "1" : "0");
    }
    protected boolean getBool(String column) {
        return get(column, "0").equals("1");
    }
    protected int getInt(String column) {
        return Integer.parseInt(get(column, "0"));
    }
    protected long getLong(String column) {
        return Long.parseLong(get(column, "0"));
    }
    
    public ContentValues getContentValues() {
        ContentValues values = new ContentValues();
        values.put(BaseColumns._ID, String.valueOf(getId()));
        for (String col : getTable().getColumns()) {
            if (!col.equals(BaseColumns._ID)) {
                values.put(col, data.get(col));
            }
        }
        return values;
    }
    public void fillFromCursor(Cursor c) {
        data.clear();
        int cii = c.getColumnIndexOrThrow(BaseColumns._ID);
        id = c.getLong(cii);
        for (String col : getTable().getColumns()) {
            if (!col.equals(BaseColumns._ID)) {
                int ci = c.getColumnIndex(col);
                if (ci > -1) {
                    data.put(col, c.getString(ci));
                }
            }
        }
    }
}
