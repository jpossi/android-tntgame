/*******************************************************************************
 * Copyright 2014 Jascha Starke <jascha@ja-s.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.jaschastarke.android.tasks;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

abstract public class FixedAsyncLoader<D> extends AsyncTaskLoader<D> {
    private D currentData;
    public FixedAsyncLoader(Context context) {
        super(context);
    }
    
    @Override
    protected void onStartLoading() {
        if (currentData != null) {
            deliverResult(currentData);
        } else {
            forceLoad();
        }
    }
    

    /**
     * Called when there is new data to deliver to the client.  The
     * super class will take care of delivering it; the implementation
     * here just adds a little more logic.
     */
    @Override
    public void deliverResult(D data) {
        /*if (isReset()) {
            // An async query came in while the loader is stopped.  We
            // don't need the result.
            if (data != null) {
                onReleaseResources(data);
            }
        }*/
        //List<T> oldData = data;
        currentData = data;

        if (isStarted()) {
            // If the Loader is currently started, we can immediately
            // deliver its results.
            super.deliverResult(data);
        }
        /*
        // At this point we can release the resources associated with
        // 'oldApps' if needed; now that the new result is delivered we
        // know that it is no longer in use.
        if (oldData != null) {
            onReleaseResources(oldData);
        }*/
    }
    @Override
    protected void onReset() {
        super.onReset();
        currentData = null;
    }
}
