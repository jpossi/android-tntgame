/*******************************************************************************
 * Copyright 2014 Jascha Starke <jascha@ja-s.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package de.jaschastarke.android.views;

import android.content.Context;
import android.util.AttributeSet;

public class WebView extends android.webkit.WebView {
    private Runnable initScrollListener;
    
    public WebView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
    public WebView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    public WebView(Context context) {
        super(context);
    }
    
    public void setOnContentVisible(Runnable listener) {
        initScrollListener = listener;
    }
    @Override
    public void invalidate() {
        super.invalidate();
        if (initScrollListener != null && getContentHeight() > 0) {
            initScrollListener.run();
            initScrollListener = null;
        }
    }
}
