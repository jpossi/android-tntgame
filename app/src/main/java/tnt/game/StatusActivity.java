/*******************************************************************************
 * Copyright 2014 Jascha Starke <jascha@ja-s.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package tnt.game;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import tnt.game.alwayson.AlwaysOnService;
import tnt.game.api.ConnectionException;
import tnt.game.api.User;
import tnt.game.core.Activity;
import tnt.game.core.MenuListAdapter;
import tnt.game.mission.MissionEndReceiver;
import tnt.game.status.PagerAdapter;
import tnt.game.status.SelectCharActionProvider;
import tnt.game.storage.TNTContract;
import tnt.game.storage.UserEntry;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public class StatusActivity extends Activity {
    public static final String CHAR_FILENAME = "char_%d.png";
    public static final String CHAR_ICON_FILENAME = "icon_char_%d.png";
    
    private File charImage;
    private File charIconImage;
    private MenuItem refreshItem;

    // App Drawer
    DrawerLayout mDrawerLayout;
    ListView mDrawerList;
    ActionBarDrawerToggle mDrawerToggle;
    MenuListAdapter mMenuAdapter;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.shared_activity_pager);
        
        if (user() == null) {
            Toast.makeText(this, getString(R.string.error_not_loggedin), Toast.LENGTH_SHORT).show();
            gotoLogin();
            finish();
            return;
        } else {
            application().state().getPrivatePreferences().edit()
                .putLong(PREF_LAST_USER, user().getId())
                .commit();
        }
        final PagerAdapter pagerAdapter = new PagerAdapter(this);
        final ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        
        pagerAdapter.registerWithActionBar((ViewPager) findViewById(R.id.pager), actionBar);
        
        registerForCloseOnLogout();
        registerForNewMessage();

        // Get the Title
        mTitle = mDrawerTitle = user().getName();

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.listview_drawer);
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow,
                GravityCompat.START);
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.drawable.ic_drawer, R.string.drawer_open,
                R.string.drawer_close) {
      
                public void onDrawerClosed(View view) {
                    // TODO Auto-generated method stub
                    super.onDrawerClosed(view);
                }
      
                public void onDrawerOpened(View drawerView) {
                    // TODO Auto-generated method stub
                    // Set the title on the action when drawer open
                    getSupportActionBar().setTitle(mDrawerTitle);
                    super.onDrawerOpened(drawerView);
                }
            };
        actionBar.setHomeButtonEnabled(useDrawer());

        if (AlwaysOnService.mIsServiceRunning != true){
            startService(new Intent(this, AlwaysOnService.class));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (refreshItem != null && refreshItem.getActionView() != null) {
            refreshItem.getActionView().clearAnimation();
            refreshItem.setActionView(null);
        }
        
        getSupportMenuInflater().inflate(R.menu.status, menu);
        refreshItem = menu.findItem(R.id.action_refresh);
        
        final MenuItem item = menu.findItem(R.id.action_switch_login);
        item.setVisible(useDropDown());
        new AsyncTask<Void, Void, Drawable>() {
            private List<UserEntry> availableUsers;
            @Override
            protected Drawable doInBackground(Void... params) {
                availableUsers = new ArrayList<UserEntry>();
                synchronized (container().db()) {
                    Cursor query = container().db().getWritableDatabase().query(TNTContract.UserTable.TABLE_NAME,
                            new String[]{TNTContract.UserTable._ID, TNTContract.UserTable.COLUMN_NAME_USERNAME},
                            TNTContract.UserTable.COLUMN_NAME_HASH + " <> ''",
                            null, null, null, null);
                    while (query.moveToNext()) {
                        availableUsers.add(TNTContract.USER.createEntry(query));
                    }
                    query.close();
                    container().db().close();
                }
                
                File file = getIconImageFile();
                if (file == null)
                    return null;
                Bitmap bitmap;
                try {
                    bitmap = BitmapFactory.decodeStream(new FileInputStream(file));
                } catch (FileNotFoundException e) {
                    Log.e("tnt.game", "Exception: couldn't read " + file, e);
                    return null;
                }
                return new BitmapDrawable(getResources(), bitmap);
            }
            @Override
            protected void onPostExecute(Drawable result) {
                if (result != null)
                    container().state().storeLoggedinUser(user(), result);
                if (useDrawer() == true){
                    mMenuAdapter = new MenuListAdapter(StatusActivity.this, availableUsers);
                    mDrawerList.setAdapter(mMenuAdapter);
                    mDrawerList.setItemChecked(mMenuAdapter.getItemPosition(user().getId()), true);
                }
                if (useDropDown() == true){
                SelectCharActionProvider actionProvider = new SelectCharActionProvider(StatusActivity.this, availableUsers);
                actionProvider.setActionProviderTo(item);
            }
                setTitle(mTitle);
            }
        }.execute();
        
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (item.getGroupId() == SelectCharActionProvider.ITEM_GROUP_ID) {
            if (container().state().getActiveUser(item.getItemId()) != null) {
                Intent intent = new Intent(this, StatusActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra(Activity.EXTRA_USER_ID, (long) item.getItemId());
                startActivity(intent);
            } else {
                Intent intent = new Intent(this, LoginActivity.class);
                intent.putExtra(LoginActivity.EXTRA_SWITCH_LOGIN, (long) item.getItemId());
                startActivity(intent);
            }
            return true;
        } else {
            switch (item.getItemId()) {
                case SelectCharActionProvider.ITEM_ID_ADD:
                    Intent intent = new Intent(this, LoginActivity.class);
                    intent.putExtra(LoginActivity.EXTRA_ADDITIONAL_LOGIN, true);
                    startActivity(intent);
                    return true;
                case R.id.action_refresh:
                    doRefresh(item);
                    return true;
                case android.R.id.home:
                    if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
                        mDrawerLayout.closeDrawer(mDrawerList);
                    } else {
                        mDrawerLayout.openDrawer(mDrawerList);
                    }
                    return super.onOptionsItemSelected(item);
                default:
                    return super.onOptionsItemSelected(item);
            }
        }
    }
    
    protected void doRefresh(final MenuItem item) {
        loading(true);
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ImageView iv = (ImageView) inflater.inflate(R.layout.action_refresh, null);

        Animation rotation = AnimationUtils.loadAnimation(this, R.anim.clockwise_refresh);
        rotation.setRepeatCount(Animation.INFINITE);
        iv.startAnimation(rotation);

        item.setActionView(iv);
        
        setTitle(user().getName());
        
        new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... params) {
                try {
                    userApi().refresh();
                } catch (ConnectionException e) {
                    return false;
                }
                return true;
            }
            @Override
            protected void onPostExecute(Boolean result) {
                if (!result) {
                    Toast.makeText(StatusActivity.this, getString(R.string.error_connection_error), Toast.LENGTH_SHORT).show();
                } else {
                    charImage = null;
                    charIconImage = null;
                    PagerAdapter adapter = (PagerAdapter) ((ViewPager) findViewById(R.id.pager)).getAdapter();
                    adapter.doRefresh();
                }
                if (item.getActionView() != null) {
                    item.getActionView().clearAnimation();
                    item.setActionView(null);
                }
                loading(false);
            }
        }.execute();
    }
    
    /**
     * @Waits
     */
    public File getImageFile() {
        if (charImage == null) {
            File file = new File(getFilesDir(), String.format(Locale.US, CHAR_FILENAME, user().getId()));
            File iconFile = new File(getFilesDir(), String.format(Locale.US, CHAR_ICON_FILENAME, user().getId()));
            
            User api = container().api().getUserAPI(user());
            boolean changed = api.cacheImage(file);
            if (changed) {
                user().setUserImg(api.getImage());
                synchronized (container().db()) {
                    container().db().update(user());
                    container().db().close();
                }
            }
            if (!file.exists()) {
                Log.e("tnt.game", "Error: Missing Character-Image");
                return null;
            }

            if (!iconFile.exists() || changed) {
                try {
                    Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(file));
                    if (bitmap == null)
                        return null;
                    
                    int x = 0;
                    int y = 0;
                    int width = bitmap.getWidth();
                    int height = bitmap.getHeight();
                    float density = getResources().getDisplayMetrics().density;
                    int size = Math.min((int) Math.floor(height / 2), width);
                    if (size < width) {
                        x = (int) Math.floor((width - size) / 2);
                    }
                    
                    int targetSize = (int) (32 * density);
                    Bitmap tmp = Bitmap.createBitmap(bitmap, x, y, size, size);
                    Bitmap icon = Bitmap.createScaledBitmap(tmp, targetSize, targetSize, true);
                    
                    FileOutputStream iconOutput = new FileOutputStream(iconFile);
                    icon.compress(Bitmap.CompressFormat.PNG, 100, iconOutput);
                    iconOutput.close();
                } catch (IOException e) {
                    Log.e("tnt.game", "Exception: Couldn't create icon_char.png", e);
                }
            }
            charImage = file;
            charIconImage = iconFile;
        }
        return charImage;
    }
    
    /**
     * @Waits
     */
    public File getIconImageFile() {
        getImageFile();
        return charIconImage;
    }
    
    private void status_increase(User.Stats s){
        loading(true);
        new AsyncTask<User.Stats, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(User.Stats... params) {
                return userApi().doStatInc(params[0]) == 1;
            }
            @Override
            protected void onPostExecute(Boolean result) {
                if (!result) {
                    Toast.makeText(StatusActivity.this, getString(R.string.not_enough_gold), Toast.LENGTH_SHORT).show();
                    loading(false);
                }else {
                    doRefresh(refreshItem);
                }
            }
        }.execute(s);
    }

    public void status_strength_onClick(View v){
        status_increase(User.Stats.MAIN);
    }
    public void status_defense_onClick(View v){
        status_increase(User.Stats.DEFENSE);
    }
    public void status_life_onClick(View v){
        status_increase(User.Stats.LIFE);
    }
    public void status_luck_onClick(View v){
        status_increase(User.Stats.LUCK);
    }
    
    protected void loading(boolean show) {
        if (show) {
            findViewById(R.id.loading_spinner).setVisibility(View.VISIBLE);
            findViewById(R.id.stats).setVisibility(View.GONE);
        } else {
            findViewById(R.id.loading_spinner).setVisibility(View.GONE);
            findViewById(R.id.stats).setVisibility(View.VISIBLE);
        }
    }
    
    // ListView click listener in the navigation drawer
    private class DrawerItemClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                long id) {
            selectItem(position);
        }
    }
    
    private void selectItem(int position) {
        if(position != mMenuAdapter.getItemPosition(user().getId())){
            if(((UserEntry) mMenuAdapter.getItem(position)).getId() != 999){
                if (container().state().getActiveUser(((UserEntry) mMenuAdapter.getItem(position)).getId()) != null) {
                    Intent intent = new Intent(this, StatusActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra(Activity.EXTRA_USER_ID, (long) ((UserEntry) mMenuAdapter.getItem(position)).getId());
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(this, LoginActivity.class);
                    intent.putExtra(LoginActivity.EXTRA_SWITCH_LOGIN, (long) ((UserEntry) mMenuAdapter.getItem(position)).getId());
                    startActivity(intent);
                }
            }
            else {
                Intent intent = new Intent(this, LoginActivity.class);
                intent.putExtra(LoginActivity.EXTRA_ADDITIONAL_LOGIN, true);
                startActivity(intent);
            }
        }

        // Close drawer
        mDrawerLayout.closeDrawer(mDrawerList);
    }
    
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }
    
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles
        mDrawerToggle.onConfigurationChanged(newConfig);
    }
    
    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getSupportActionBar().setTitle(mTitle);
        setIcon();
    }

    public void setIcon() {
        getIconImageFile();
        Bitmap bitmap;
        try {
            bitmap = BitmapFactory.decodeStream(new FileInputStream(charIconImage));
            getSupportActionBar().setIcon(new BitmapDrawable(this.getResources(), bitmap));
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void setDrawerSwipe(boolean b){
        if (useDrawer() == true){
            if (b == true){
                mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            } else {
                mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            }
        }
    }

    public boolean useDrawer(){
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        if (Integer.parseInt(pref.getString(PreferencesActivity.KEY_MENU_LAYOUT_CHOICE, "1").toString()) != 1) {
            return true;
        } else {
            return false;
        }
    }

    public boolean useDropDown(){
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        if (Integer.parseInt(pref.getString(PreferencesActivity.KEY_MENU_LAYOUT_CHOICE, "1").toString()) != 2) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first
        if (mDrawerLayout.isDrawerOpen(mDrawerList)) { mDrawerLayout.closeDrawer(mDrawerList); }
        getSupportActionBar().setHomeButtonEnabled(useDrawer());
        invalidateOptionsMenu();
    }
}
