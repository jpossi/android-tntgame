/*******************************************************************************
 * Copyright 2014 Jascha Starke <jascha@ja-s.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package tnt.game.mission;

import tnt.game.MissionsActivity;
import tnt.game.PreferencesActivity;
import tnt.game.R;
import tnt.game.core.Activity;
import tnt.game.core.Container;
import tnt.game.storage.ActivityEntry;
import tnt.game.storage.TNTContract;
import tnt.game.storage.TNTContract.ActivityTable;
import tnt.game.storage.UserEntry;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

public class NotificationService extends IntentService {
    public NotificationService() {
        super("NotificationService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d("tnt.game", "NotificationService: begin");
        Container container = Container.getForContext(getApplicationContext());
        SQLiteDatabase db = container.db().getWritableDatabase();
        
        long time = (System.currentTimeMillis() / 1000) + 3;
        
        // Fetch all finished missions to notify
        Cursor c = db.query(ActivityTable.TABLE_NAME,
                ActivityTable.COLUMNS,
                ActivityTable.COLUMN_NAME_TIME_END + " < ? AND " +
                    ActivityTable.COLUMN_NAME_NOTIFIED + " = 0",
                new String[]{String.valueOf(time)},
                null, null,  null);
        while (c.moveToNext()) {
            NotificationManager notifyMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            ActivityEntry entry = TNTContract.ACTIVITY.createEntry(c);
            UserEntry user = container.db().find(TNTContract.USER, entry.getUserId());
            if (user == null)
                continue;
            Log.d("tnt.game", "NotificationService: Notification Mission: " + entry.getId());
            
            Intent clickIntent = new Intent(this, MissionsActivity.class);
            clickIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            clickIntent.putExtra(Activity.EXTRA_USER_ID, user.getId());
            PendingIntent pendingIntent = PendingIntent.getActivity(this, (int) entry.getId(), clickIntent, 0);
            
            int min = entry.getDuration() / 60;
            
            String text = getString(R.string.character) + " " + user.getName();
            if (entry.getName() != null && entry.getName().length() > 0)
                text += " - " + entry.getName();
            
            NotificationCompat.Builder notification = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(getString(R.string.mission_complete_min, min))
                .setContentText(text)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .setOnlyAlertOnce(true)
                .setDefaults(Notification.DEFAULT_SOUND);

            SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
            if (pref.getBoolean(PreferencesActivity.KEY_NOTIFICATION_ENABLED, true)) {
                notification.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);
            }
            
            entry.flagNotified();
            container.db().update(entry);
            
            if (MissionsActivity.active != user.getId()) {
                notifyMgr.notify((int) entry.getId(), notification.build());
            }
        }
        c.close();
        container.db().close();
        
        MissionEndReceiver.scheduleAlarm(this);
        
        MissionEndReceiver.completeWakefulIntent(intent);
    }

}
