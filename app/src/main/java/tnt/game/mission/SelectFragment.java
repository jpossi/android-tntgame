/*******************************************************************************
 * Copyright 2014 Jascha Starke <jascha@ja-s.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package tnt.game.mission;

import java.util.List;

import tnt.game.MissionsActivity;
import tnt.game.R;
import tnt.game.api.BattleReport;
import tnt.game.api.ConnectionException;
import tnt.game.api.Hunt;
import tnt.game.api.Missions;
import tnt.game.api.Missions.MissionHeader;
import tnt.game.api.Missions.State;
import tnt.game.core.Container;
import tnt.game.hunt.RewardFragment;
import tnt.game.storage.ActivityEntry;
import tnt.game.storage.ActivityEntry.Type;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

public class SelectFragment extends ListFragment {
    private MissionAdapter missionAdapter;
    private AsyncTask<Void, Void, State> task;
    
    protected MissionsActivity act() {
        return ((MissionsActivity) getActivity());
    }
    protected Container container() {
        return act().container();
    }
    protected Missions api() {
        return container().api().getUserAPI(act().user()).getMissionsAPI();
    }
    /*
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.mission_fragment_list, container, false);
    }*/

    private class MissionTask extends AsyncTask<Void, Void, Missions.State> {
        protected ActivityEntry activityEntry;
        private boolean error = false;
        @Override
        protected Missions.State doInBackground(Void... params) {
            synchronized (container().db()) {
                activityEntry = container().db().findActivity(act().user());
                if (activityEntry != null && !activityEntry.isOutdated()) {
                    if (activityEntry.getType() == Type.BOSSBATTLE) {
                        container().db().close();
                        return State.IN_BOSSBATTLE;
                    } else if (activityEntry.getType() == Type.MISSION) {
                        container().db().close();
                        return State.IN_WORK;
                    }
                }
                container().db().close();
            }
            if (isCancelled())
                return null;
            Missions.State result;
            try {
                result = api().fetchCurrentState();
            } catch (ConnectionException e) {
                error = true;
                return null;
            }
            if (isCancelled())
                return null;
            synchronized (container().db()) {
                if (result == State.IN_BOSSBATTLE) {
                    int bosstimer = container().api().getUserAPI(act().user()).getBossAPI().getBossTimer();
                    activityEntry = container().db().activityHelper(act().user()).bossFightToEndIn(activityEntry, bosstimer);
                } else if (result == State.HUNT_ACTIVE) {
                    Hunt api = container().api().getUserAPI(act().user()).getHuntAPI();
                    activityEntry = container().db().activityHelper(act().user()).huntToEndIn(activityEntry, api.getTimer(), api.getCurrentOpponent());
                } else if (result == State.IN_WORK) {
                    activityEntry = container().db().activityHelper(act().user()).missionToEndIn(activityEntry, api().getTimer());
                    MissionEndReceiver.scheduleAlarm(getActivity());
                } else if (activityEntry != null) {
                    container().db().delete(activityEntry);
                }
                container().db().close();
            }
            return result;
        }
        @Override
        protected void onPostExecute(Missions.State result) {
            task = null;
            if (getActivity() != null && !isCancelled()) {
                if (error) {
                    Toast.makeText(getActivity(), getString(R.string.error_connection_error), Toast.LENGTH_SHORT).show();
                    getActivity().finish();
                    return;
                } else if (result != null && getActivity() != null) {
                    switch (result) {
                        case MISSION_CHOICE:
                            missionAdapter.setMissionList(api().getMissions());
                            if (getView() != null)
                                setListShown(true);
                            break;
                        case IN_WORK:
                            act().displayInWork(activityEntry);
                            break;
                        case HUNT_ACTIVE:
                            Toast.makeText(getActivity(), getString(R.string.error_currently_hunting), Toast.LENGTH_SHORT).show();
                            act().back();
                            break;
                        case HUNT_FINISHED:
                            new RewardFragment().loadRewards(act().userApi().getHuntAPI()).show(act().getSupportFragmentManager(), "HUNT_REWARD");
                            task = new MissionTask().execute();
                            break;
                        case IN_BOSSBATTLE:
                            Toast.makeText(getActivity(), getString(R.string.error_bossbattle_active), Toast.LENGTH_SHORT).show();
                            act().back();
                            break;
                        case BATTLE_REPORT:
                            BattleReport currentReport = api().getBattleReport();
                            if (currentReport != null) {
                                act().displayBattleReportNotification(currentReport);
                            }
                            task = new MissionTask().execute();
                            break;
                    }
                }
            }
        }
    }
    
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        missionAdapter = new MissionAdapter(getActivity());
        setListAdapter(missionAdapter);

        List<MissionHeader> missions = null;
        if (savedInstanceState != null) {
            if (api().getTimer() != null)
                return;
            else
                missions = api().getMissions();
        }
        if (missions != null) {
            missionAdapter.setMissionList(missions);
            if (getView() != null)
                setListShown(true);
        } else {
            fillMissions();
        }
    }
    
    public void fillMissions() {
        if (task != null)
            return;
        missionAdapter.setMissionList(null);
        if (getView() != null)
            setListShown(false);
        
        if (getActivity() != null)
            task = new MissionTask().execute();
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (task != null) {
            task.cancel(false);
            task = null;
        }
    }

    @Override
    public void onListItemClick(final ListView l, final View v, final int position, final long id) {
        act().selectMission(missionAdapter.getItem(position));
    }
}
