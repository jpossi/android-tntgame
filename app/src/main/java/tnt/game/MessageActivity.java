/*******************************************************************************
 * Copyright 2014 Jascha Starke <jascha@ja-s.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package tnt.game;

import java.util.EnumMap;
import java.util.Map;

import tnt.game.api.ConnectionException;
import tnt.game.api.Messages;
import tnt.game.api.Messages.BoxType;
import tnt.game.api.SessionTimeoutException;
import tnt.game.core.Activity;
import tnt.game.core.TabFragmentPagerAdapter;
import tnt.game.messages.BoxFragment;
import tnt.game.messages.DisplayFragment;
import tnt.game.messages.NewMessageActivity;
import tnt.game.messages.UpdateMessageListTask;
import tnt.game.storage.MessageEntry;
import tnt.game.storage.TNTContract;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentManager.OnBackStackChangedListener;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public class MessageActivity extends Activity implements OnBackStackChangedListener {
    private static final int REQUESTCODE_REFRESH = 2;
    private static final String BACKSTACK_DISPLAY = "BACKSTACK_DISPLAY";
    private Map<BoxType, BoxFragment> boxes = new EnumMap<BoxType, BoxFragment>(BoxType.class);
    private DisplayFragment display;
    private TabFragmentPagerAdapter adapter;
    //private BoxesFragment boxes;
    private MenuItem refreshAction;
    private boolean loading;
    private AsyncTask<?, ?, ?> updateTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.activity_messages);

        if (user() == null) {
            Toast.makeText(this, getString(R.string.error_not_loggedin), Toast.LENGTH_SHORT).show();
            gotoLogin();
            finish();
            return;
        }
        
        // Show the Up button in the action bar.
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setIcon(container().state().getUserIcon(user()));
        
        adapter = new TabFragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public void onEntrySelected(int position) {
                BoxFragment box = boxes.get(BoxType.get(position));
                if (box != null)
                    box.onPageSelected();
            }

            @Override
            public int getCount() {
                return BoxType.values().length;
            }
            
            @Override
            public BoxFragment getItem(int position) {
                BoxType t = BoxType.get(position);
                if (boxes.containsKey(t)) {
                    return boxes.get(t);
                } else {
                    BoxFragment f = new BoxFragment();
                    Bundle args = new Bundle();
                    args.putSerializable(BoxFragment.ARGS_BOX_TYPE, t);
                    f.setArguments(args);
                    boxes.put(t, f);
                    return f;
                }
            }
            
            @Override
            public String getPageTitle(int position) {
                switch (BoxType.get(position)) {
                    case INBOX:
                        return getString(R.string.message_inbox);
                    case OUTBOX:
                        return getString(R.string.message_outbox);
                    case INFORMATION:
                        return "Information";
                }
                return super.getPageTitle(position).toString();
            }
        };
        
        getSupportFragmentManager().addOnBackStackChangedListener(this);
        ViewPager pager = (ViewPager) findViewById(R.id.pager);
        
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            getSupportActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
            
            adapter.registerWithActionBar(pager, getSupportActionBar());
            pager.post(new Runnable() {
                @Override
                public void run() {
                    BoxType type = BoxType.get(getSupportActionBar().getSelectedNavigationIndex());
                    BoxFragment box = boxes.get(type);
                    if (box != null) {
                        box.onPageSelected();
                        updateList(type, true);
                    }
                }
            });
        } else {
            display = (DisplayFragment) getSupportFragmentManager().findFragmentByTag(BACKSTACK_DISPLAY);
            adapter.bindToViewPager(pager);
            onBackStackChanged();
        }
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent resultData) {
        if (requestCode == REQUESTCODE_REFRESH && resultCode == RESULT_OK)
            updateList(BoxType.OUTBOX, true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (refreshAction != null && refreshAction.getActionView() != null) {
            refreshAction.getActionView().clearAnimation();
            refreshAction.setActionView(null);
        }
        
        getSupportMenuInflater().inflate(R.menu.messages, menu);
        
        refreshAction = menu.findItem(R.id.action_refresh);
        
        boolean indisplay = display != null && display.getMessage() != null;
        MenuItem newMessage = menu.findItem(R.id.action_new_message);
        MenuItem deleteAll = menu.findItem(R.id.action_delete_all);
        MenuItem delete = menu.findItem(R.id.action_delete);
        MenuItem reply = menu.findItem(R.id.action_reply);
        if (refreshAction != null)
            refreshAction.setVisible(!indisplay);
        if (deleteAll != null)
            deleteAll.setVisible(!indisplay);
        if (newMessage != null)
            newMessage.setVisible(!indisplay);
        if (delete != null)
            delete.setVisible(indisplay);
        if (reply != null)
            reply.setVisible(indisplay);
        
        indicateLoading(loading);
        return true;
    }
    
    public void indicateLoading(boolean on) {
        // Before onCreate:setContentView
        //requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        
        //setSupportProgressBarIndeterminate(true);
        //setSupportProgressBarIndeterminateVisibility(false);
        loading = on;
        if (refreshAction != null) {
            if (on && refreshAction.getActionView() == null) {
                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                ImageView iv = (ImageView) inflater.inflate(R.layout.action_refresh, null);
                
                Animation rotation = AnimationUtils.loadAnimation(this, R.anim.clockwise_refresh);
                rotation.setRepeatCount(Animation.INFINITE);
                iv.startAnimation(rotation);
                
                refreshAction.setActionView(iv);
                refreshAction.setEnabled(false);
            } else if (!on && refreshAction.getActionView() != null) {
                refreshAction.getActionView().clearAnimation();
                refreshAction.setActionView(null);
                refreshAction.setEnabled(true);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_reply:
                if (display != null && display.getMessage() != null) {
                    final MessageEntry message = display.getMessage();
                    
                    String subject = getString(R.string.message_reply_subject, message.getSubject());
                    
                    Intent rmIntent = new Intent(this, NewMessageActivity.class);
                    rmIntent.putExtra(Activity.EXTRA_USER_ENTRY, user());
                    rmIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{message.getContact()});
                    rmIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
                    startActivityForResult(rmIntent, REQUESTCODE_REFRESH);
                    return true;
                }
                return true;
            case R.id.action_delete_all:
                final BoxType type = BoxType.get(getSupportActionBar().getSelectedNavigationIndex());
                new AsyncTask<Void, Void, Boolean>() {
                    @Override
                    protected void onPreExecute() {
                        indicateLoading(true);
                    }
                    @Override
                    protected Boolean doInBackground(Void... params) {
                        try {
                            boolean success = userApi().getMessagesAPI().deleteAll(type);
                            if (success) {
                                SQLiteDatabase db = container().db().getWritableDatabase();
                                db.delete(TNTContract.MessageTable.TABLE_NAME,
                                        TNTContract.MessageTable.COLUMN_NAME_USER_ID + " = ? AND " +
                                            TNTContract.MessageTable.COLUMN_NAME_TYPE + " = ?",
                                        new String[]{String.valueOf(user().getId()), String.valueOf(type.ordinal())});
                                db.close();
                            }
                            return success;
                        } catch (SessionTimeoutException e) {
                        } catch (ConnectionException e) {
                        }
                        return false;
                    }
                    @Override
                    protected void onPostExecute(Boolean result) {
                        indicateLoading(false);
                        updateList(type, false);
                    }
                }.execute();
                return true;
            case R.id.action_delete:
                if (display != null && display.getMessage() != null) {
                    final MessageEntry message = display.getMessage();
                    new AsyncTask<MessageEntry, Void, Boolean>() {
                        @Override
                        protected void onPreExecute() {
                            getSupportFragmentManager().popBackStackImmediate(BACKSTACK_DISPLAY, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                            indicateLoading(true);
                        }
                        @Override
                        protected Boolean doInBackground(MessageEntry... params) {
                            try {
                                MessageEntry entry = params[0];
                                boolean success = container().api().getUserAPI(user()).getMessagesAPI().deleteMessage(entry);
                                if (success) {
                                    container().db().delete(entry);
                                }
                                return success;
                            } catch (ConnectionException e) {
                                return false;
                            }
                        }
                        @Override
                        protected void onPostExecute(Boolean result) {
                            if (!result) {
                                Toast.makeText(MessageActivity.this, getString(R.string.error_connection_error), Toast.LENGTH_SHORT).show();
                            } else {
                                indicateLoading(false);
                                updateList(message.getType(), false);
                            }
                        }
                    }.execute(message);
                }
                return true;
            case R.id.action_new_message:
                Intent nmIntent = new Intent(this, NewMessageActivity.class);
                nmIntent.putExtra(Activity.EXTRA_USER_ENTRY, user());
                startActivityForResult(nmIntent, REQUESTCODE_REFRESH);
                return true;
            case R.id.action_refresh:
                int index = getSupportActionBar().getSelectedNavigationIndex();
                updateList(BoxType.get(index), true);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    
    public void viewMessage(MessageEntry msg) {
        if (display == null)
            display = new DisplayFragment();
        Bundle args = new Bundle();
        args.putSerializable(DisplayFragment.ARGUMENT_MESSAGE, msg);
        display.setArguments(args);
        getSupportFragmentManager().beginTransaction()
            .add(R.id.fragment_container, display, BACKSTACK_DISPLAY)
            .addToBackStack(BACKSTACK_DISPLAY)
            .commit();
    }

    @Override
    public void onBackStackChanged() {
        boolean indisplay = getSupportFragmentManager().getBackStackEntryCount() > 0;
        findViewById(R.id.pager).setVisibility(indisplay ? View.GONE : View.VISIBLE);
        findViewById(R.id.fragment_container).setVisibility(indisplay ? View.VISIBLE : View.GONE);
        
        if (indisplay) {
            adapter.unregisterFromActionbar();
            getSupportActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        } else {
            getSupportActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
            adapter.registerWithActionBar(getSupportActionBar());
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (updateTask != null) {
            updateTask.cancel(false);
            updateTask = null;
        }
    }

    public void updateList(final BoxType type, boolean fullUpdate) {
        final BoxFragment box = boxes.get(type);
        if (box != null) {
            if (fullUpdate) {
                updateTask = new UpdateMessageListTask(this) {
                    @Override
                    protected void onPreExecute() {
                        super.onPreExecute();
                        indicateLoading(true);
                    }

                    @Override
                    protected void onPostExecute(Boolean result) {
                        super.onPostExecute(result);
                        indicateLoading(false);
                        updateTask = null;
                        box.update();
                    }
                }.execute();
            } else {
                box.update();
            }
        }
    }
}
