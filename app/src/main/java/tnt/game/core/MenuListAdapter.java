package tnt.game.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Locale;

import tnt.game.R;
import tnt.game.storage.UserEntry;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
 
public class MenuListAdapter extends BaseAdapter implements ListAdapter {
 
	// Declare Variables
	Context context;
	private List<UserEntry> mUserList;
	int[] mIcon;
	LayoutInflater inflater;
 
	public MenuListAdapter(Context context, List<UserEntry> list) {
		this.context = context;
		this.mUserList = list;
		// add new user
		UserEntry add = new UserEntry();
		add.setId(999);
		this.mUserList.add(add);
	}
 
	@Override
	public int getCount() {
		return mUserList.size();
	}
 
	@Override
	public Object getItem(int position) {
		return mUserList.get(position);
	}
 
	@Override
	public long getItemId(int position) {
		return position;
	}
 
	public View getView(int position, View convertView, ViewGroup parent) {
		// Declare Variables
		TextView txtTitle;
 
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View itemView = inflater.inflate(R.layout.drawer_list_item, parent,
				false);
 
		// Locate the TextViews in drawer_list_item.xml
		txtTitle = (TextView) itemView.findViewById(R.id.title);
  

		if (((UserEntry) getItem(position)).getId() != 999){
			txtTitle.setText(mUserList.get(position).getName());
			setUserImage(position, itemView);
		} else {
			txtTitle.setText(R.string.add_login);
			ImageView imgIcon = (ImageView) itemView.findViewById(R.id.icon);
			imgIcon.setImageResource(R.drawable.ic_action_new);
		}
		
		return itemView;
	}
	
	private void setUserImage(final int postition, View itemView){
		final ImageView imgIcon = (ImageView) itemView.findViewById(R.id.icon);
		
		new AsyncTask<Void, Void, BitmapDrawable>() {
            @Override
            protected BitmapDrawable doInBackground(Void... params) {
                File file = new File(context.getFilesDir(), String.format(Locale.US, "icon_char_%d.png", mUserList.get(postition).getId()));
                if (file.exists()) {
                    try {
                        Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(file));
                        return new BitmapDrawable(context.getResources(), bitmap);
                    } catch (FileNotFoundException e) {
                        Log.e("tnt.game", "Exception: couldn't read " + file, e);
                    }
                }
                return null;
            }
            @Override
            protected void onPostExecute(BitmapDrawable result) {
                if (result != null) {
                	imgIcon.setImageDrawable(result);
                }
            }
        }.execute();
	}
 
	public int getItemPosition(long id){
		for (UserEntry user: mUserList){
			if(user.getId() == id){ return mUserList.indexOf(user); }
		}
		return getCount();
	}
}