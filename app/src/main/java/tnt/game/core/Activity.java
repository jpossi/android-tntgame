/*******************************************************************************
 * Copyright 2014 Jascha Starke <jascha@ja-s.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package tnt.game.core;

import java.io.File;
import java.util.Locale;

import tnt.game.LoginActivity;
import tnt.game.MessageActivity;
import tnt.game.PreferencesActivity;
import tnt.game.R;
import tnt.game.StatusActivity;
import tnt.game.api.User;
import tnt.game.login.LogoutReceiver;
import tnt.game.messages.MessageReceiver;
import tnt.game.storage.UserEntry;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public class Activity extends SherlockFragmentActivity {
    public static final String EXTRA_USER_ENTRY = "USER_ENTRY";
    public static final String EXTRA_USER_ID = "USER_ID";
    public static final String EXTRA_USER_NAME = "USER_NAME";
    
    public static final String PREF_LAST_USER = "PREF_LAST_USER";
    
    private LogoutReceiver logoutReceiver;
    private MessageReceiver messageReceiver;
    private UserEntry currentUser;
    
    public TNTApplication application() {
        return (TNTApplication) getApplication();
    }
    public Container container() {
        return Container.getForContext(getApplicationContext());
    }
    public UserState userState() {
        return container().userState(user());
    }
    
    /*@Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        
        if (currentUser != null) {
            // Check for restore after application cleanup
            if (container().state().getActiveUsers().size() == 0) {
                gotoLogin();
                finish();
            }
        }
    }*/
    public UserEntry user() {
        if (currentUser == null) {
            if (getIntent().hasExtra(EXTRA_USER_ENTRY)) {
                currentUser = (UserEntry) getIntent().getSerializableExtra(EXTRA_USER_ENTRY);
            } else if (getIntent().hasExtra(EXTRA_USER_ID)) {
                currentUser = container().state().getActiveUser(getIntent().getLongExtra(EXTRA_USER_ID, -1));
            } else if (getIntent().hasExtra(EXTRA_USER_NAME)) {
                currentUser = container().state().getActiveUser(getIntent().getStringExtra(EXTRA_USER_NAME));
            }
            if (currentUser == null)
                Log.i("tnt.game", "Activity: " + this.getClass().getSimpleName() + " without User");
            else
                Log.i("tnt.game", "Activity: " + this.getClass().getSimpleName() + " with User: " + currentUser.getName());
        }
        return currentUser;
    }
    public void user(UserEntry entry) {
        currentUser = entry;
    }
    public User userApi() {
        return container().api().getUserAPI(user());
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        currentUser = null;
    }
    protected void registerForCloseOnLogout() {
        logoutReceiver = new LogoutReceiver(this);
        container().register(logoutReceiver, LogoutReceiver.FILTER);
    }
    protected void registerForNewMessage() {
        messageReceiver = new MessageReceiver(this);
        container().register(messageReceiver, MessageReceiver.FILTER);
    }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
        currentUser = null;
        if (logoutReceiver != null) {
            container().unregister(logoutReceiver);
            logoutReceiver = null;
        }
        if (messageReceiver != null) {
            container().unregister(messageReceiver);
            messageReceiver = null;
        }
    }
    
    protected void logout() {
        final UserEntry userEntry = user();
        userEntry.setHash(null);
        container().state().removeLoggedinUser(userEntry);
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                File[] files = new File[2];
                files[0] = new File(getFilesDir(), String.format(Locale.US, StatusActivity.CHAR_FILENAME, userEntry.getId()));
                files[1] = new File(getFilesDir(), String.format(Locale.US, StatusActivity.CHAR_ICON_FILENAME, userEntry.getId()));
                for (File file : files) {
                if (file.exists())
                    file.delete();
                }
                container().db().removeUser(userEntry);
                container().db().close();
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                gotoLogin();
            }
        }.execute();
    }
    
    protected void gotoLogin() {
        final UserEntry userEntry = user();
        Intent intent = new Intent(Activity.this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        Intent intent2 = new Intent();
        intent2.setAction(LogoutReceiver.LOGOUT_ACTION);
        if (userEntry != null)
            intent2.putExtra(EXTRA_USER_ID, userEntry.getId());
        container().broadcast(intent2);
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if ((getSupportActionBar().getDisplayOptions() & ActionBar.DISPLAY_HOME_AS_UP) != 0) {
                    Intent intent = NavUtils.getParentActivityIntent(this);
                    if (currentUser != null)
                        intent.putExtra(EXTRA_USER_ID, currentUser.getId());
                    NavUtils.navigateUpTo(this, intent);
                    return true;
                } else {
                    return super.onOptionsItemSelected(item);
                }
            case R.id.action_logout:
                logout();
                return true;
            case R.id.action_settings:
                Intent intent = new Intent(this, PreferencesActivity.class);
                startActivity(intent);
                return true;
            case R.id.statusaction_new_message:
                Intent mIntent = new Intent(this, MessageActivity.class);
                if (user() != null)
                    mIntent.putExtra(EXTRA_USER_ENTRY, user());
                mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(mIntent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean ret = super.onPrepareOptionsMenu(menu);
        
        MenuItem newMessageItem = menu.findItem(R.id.statusaction_new_message);
        if (newMessageItem != null && messageReceiver != null) {
            boolean showNewMessage = false;
            if (user() != null) {
                showNewMessage = userApi().hasNewMessages();
            }
            newMessageItem.setVisible(showNewMessage);
        }
        return ret;
    }
}
