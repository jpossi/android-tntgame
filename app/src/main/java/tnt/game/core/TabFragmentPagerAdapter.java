/*******************************************************************************
 * Copyright 2014 Jascha Starke <jascha@ja-s.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package tnt.game.core;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.TabListener;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;

abstract public class TabFragmentPagerAdapter extends FragmentPagerAdapter implements TabListener {
    public TabFragmentPagerAdapter(FragmentManager fm) {
        super(fm);
    }
    private ViewPager viewPager;
    private ActionBar actionBar;
    private int selectedItem = -1;
    
    public int getSelectedItem() {
        return selectedItem;
    }
    
    public void bindToViewPager(final ViewPager thePager) {
        viewPager = thePager;
        viewPager.setAdapter(this);
        viewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                // When swiping between different app sections, select the corresponding tab.
                if (actionBar != null)
                    actionBar.setSelectedNavigationItem(position);
                if (selectedItem != position) {
                    selectedItem = position;
                    onEntrySelected(position);
                }
            }
        });
        viewPager.setCurrentItem(getDefaultPage());
    }
    /**
     * Binds to a Pager and registers with an actionbar
     */
    public void registerWithActionBar(final ViewPager thePager, final ActionBar theActionBar) {
        bindToViewPager(thePager);
        registerWithActionBar(theActionBar);
    }
    
    public void registerWithActionBar(final ActionBar theActionBar) {
        if (actionBar != null)
            throw new IllegalStateException("Already registered to an actionBar");
        actionBar = theActionBar;
        for (int i = 0; i < viewPager.getAdapter().getCount(); i++) {
            actionBar.addTab(actionBar.newTab()
                    .setText(this.getPageTitle(i))
                    .setTabListener(this),
                false);
        }
        
        actionBar.setSelectedNavigationItem(viewPager.getCurrentItem());
    }
    
    public void unregisterFromActionbar() {
        if (actionBar != null) {
            actionBar.removeAllTabs();
            actionBar = null;
        }
    }

    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {}
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {}
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        if (viewPager.getCurrentItem() != tab.getPosition())
            viewPager.setCurrentItem(tab.getPosition());
        if (selectedItem != tab.getPosition()) {
            selectedItem = tab.getPosition();
            onEntrySelected(tab.getPosition());
        }
    }
    
    protected int getDefaultPage() {
        return 0;
    }
    
    public void onEntrySelected(int position) {
    }
}
