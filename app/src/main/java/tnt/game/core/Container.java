/*******************************************************************************
 * Copyright 2014 Jascha Starke <jascha@ja-s.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package tnt.game.core;

import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.Map;
import java.util.WeakHashMap;

import tnt.game.api.General;
import tnt.game.storage.TNTDBHelper;
import tnt.game.storage.UserEntry;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;

import com.tntgame.api.AspxWebGame;

public class Container {
    private static final Map<Context, Container> instances = new WeakHashMap<Context, Container>();
    private CurrentState state;
    private WeakReference<Context> context;
    private General api;
    
    private Container(Context context) {
        this.context = new WeakReference<Context>(context);
    }
    
    // ApplicationContext
    public Context getContext() {
        return c();
    }
    private Context c() {
        Context c = context.get();
        if (c == null)
            throw new IllegalStateException("Outdated Container-Reference used: Context reference lost!");
        return c;
    }
    
    public void free() {
        for (Map.Entry<Context, Container> entry : instances.entrySet()) {
            if (entry.getValue() == this)
                instances.remove(entry.getKey());
        }
    }
    
    public static Container getForContext(Context context) {
        Context appContext = context.getApplicationContext();
        Container inst = instances.get(context);
        if (inst == null)
            inst = instances.get(appContext);
        if (inst == null) {
            inst = new Container(context);
            instances.put(context, inst);
            if (appContext != context)
                instances.put(appContext, inst);
        }
        return inst;
    }
    
    
    public UserState userState(UserEntry user) {
        return state().getUserState(user);
    }
    public CurrentState state() {
        if (state == null)
            state = new CurrentState(c());
        return state;
    }

    public General api() {
        if (api == null)
            api = new AspxWebGame(this);
        return api;
    }
    
    private TNTDBHelper dbh;
    public TNTDBHelper db() {
        if (dbh == null)
            dbh = new TNTDBHelper(c());
        return dbh;
    }
    
    public BroadcastReceiver register(BroadcastReceiver receiver, IntentFilter filter) {
        LocalBroadcastManager.getInstance(c()).registerReceiver(receiver, filter);
        return receiver;
    }
    public void unregister(BroadcastReceiver receiver) {
        LocalBroadcastManager.getInstance(c()).unregisterReceiver(receiver);
    }
    public void broadcast(Intent msg) {
        LocalBroadcastManager.getInstance(c()).sendBroadcast(msg);
    }
    
    private Map<Object, Void> cache = new WeakHashMap<Object, Void>();
    @Deprecated// do we need it?
    public void cacheInstance(Object inst) {
        for (Iterator<Map.Entry<Object, Void>> iterator = cache.entrySet().iterator(); iterator.hasNext();) {
            if (inst.getClass().isInstance(iterator.next().getKey()))
                iterator.remove();
        }
        cache.put(inst, null);
    }
    @SuppressWarnings("unchecked")
    @Deprecated// do we need it?
    public <T> T getCachedInstance(Class<T> cls) {
        for (Object o : cache.keySet()) {
            if (cls.isInstance(o))
                return (T) o;
        }
        return null;
    }
}
