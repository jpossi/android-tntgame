/*******************************************************************************
 * Copyright 2014 Jascha Starke <jascha@ja-s.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package tnt.game.messages;

import tnt.game.core.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

public class MessageReceiver extends BroadcastReceiver {
    public static final String NEW_MESSAGE_ACTION = "tnt.game.messages.ACTION_MESSAGE_UPDATE";
    public static final IntentFilter FILTER = new IntentFilter();
    {
        FILTER.addAction(NEW_MESSAGE_ACTION);
    }
    
    private Activity act;
    
    public MessageReceiver(Activity act) {
        this.act = act;
    }
    
    @Override
    public void onReceive(Context context, Intent intent) {
        if (act.user().getId() == intent.getLongExtra(Activity.EXTRA_USER_ID, -1)) {
            act.supportInvalidateOptionsMenu();
        }
    }
}
