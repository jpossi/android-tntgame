/*******************************************************************************
 * Copyright 2014 Jascha Starke <jascha@ja-s.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package tnt.game.messages;

import tnt.game.R;
import tnt.game.api.ConnectionException;
import tnt.game.api.Messages;
import tnt.game.api.Messages.NewMessage;
import tnt.game.core.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.widget.EditText;
import android.widget.Toast;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public class NewMessageActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages_new);

        if (user() == null) {
            Toast.makeText(this, getString(R.string.error_not_loggedin), Toast.LENGTH_SHORT).show();
            gotoLogin();
            finish();
            return;
        }
        
        // Show the Up button in the action bar.
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setIcon(container().state().getUserIcon(user()));
        
        final EditText subjectField = (EditText) findViewById(R.id.message_subject);
        final EditText recipientField = (EditText) findViewById(R.id.message_recipient);
        final EditText contentField = (EditText) findViewById(R.id.message_content);
        
        if (getIntent().hasExtra(Intent.EXTRA_EMAIL)) {
            String[] r = getIntent().getStringArrayExtra(Intent.EXTRA_EMAIL);
            if (r.length == 1)
                recipientField.setText(r[0]);
        }
        if (getIntent().hasExtra(Intent.EXTRA_SUBJECT)) {
            subjectField.setText(getIntent().getStringExtra(Intent.EXTRA_SUBJECT));
        }
        
        subjectField.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                subjectField.setError(null);
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void afterTextChanged(Editable s) {}
        });
        recipientField.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                recipientField.setError(null);
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void afterTextChanged(Editable s) {}
        });
        contentField.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                contentField.setError(null);
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void afterTextChanged(Editable s) {}
        });
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getSupportMenuInflater().inflate(R.menu.messages_new, menu);
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_send_message:
                final EditText subjectField = (EditText) findViewById(R.id.message_subject);
                final EditText recipientField = (EditText) findViewById(R.id.message_recipient);
                final EditText contentField = (EditText) findViewById(R.id.message_content);
                final String subject = subjectField.getText().toString();
                final String recipient = recipientField.getText().toString();
                final String content = contentField.getText().toString();
                
                boolean error = false;
                if (recipient.length() == 0) {
                    recipientField.setError(getString(R.string.hint_required));
                    error = true;
                }
                if (content.length() == 0) {
                    contentField.setError(getString(R.string.hint_required));
                    error = true;
                }
                if (error)
                    return true;
                
                final LayoutInflater inflater = (LayoutInflater) getSupportActionBar().getThemedContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                
                subjectField.setEnabled(false);
                recipientField.setEnabled(false);
                contentField.setEnabled(false);
                item.setEnabled(false);
                item.setActionView(inflater.inflate(R.layout.shared_menu_progress, null, false));
                
                new AsyncTask<NewMessage, Void, Boolean>() {
                    private boolean exception = false;
                    @Override
                    protected Boolean doInBackground(NewMessage... params) {
                        try {
                            Messages api = container().api().getUserAPI(user()).getMessagesAPI();
                            return api.sendMessage(params[0]);
                        } catch (ConnectionException e) {
                            exception = true;
                            return false;
                        }
                    }
                    @Override
                    protected void onPostExecute(Boolean result) {
                        if (!result) {
                            subjectField.setEnabled(true);
                            recipientField.setEnabled(true);
                            contentField.setEnabled(true);
                            item.setEnabled(true);
                            item.setActionView(null);
                            
                            if (exception) {
                                Toast.makeText(NewMessageActivity.this, getString(R.string.error_connection_error), Toast.LENGTH_SHORT).show();
                            } else {
                                recipientField.setError(getString(R.string.message_player_not_found));
                            }
                        } else {
                            Toast.makeText(NewMessageActivity.this, getString(R.string.success_message_send), Toast.LENGTH_SHORT).show();
                            setResult(RESULT_OK);
                            finish();
                        }
                    }
                }.execute(new Messages.NewMessage() {
                    @Override
                    public String getSubject() {
                        return subject;
                    }
                    @Override
                    public String getRecipient() {
                        return recipient;
                    }
                    @Override
                    public String getContent() {
                        return content;
                    }
                });
                
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
