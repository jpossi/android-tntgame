/*******************************************************************************
 * Copyright 2014 Jascha Starke <jascha@ja-s.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package tnt.game.messages;

import java.text.DateFormat;

import tnt.game.MessageActivity;
import tnt.game.R;
import tnt.game.api.ConnectionException;
import tnt.game.api.Messages;
import tnt.game.api.Messages.Message;
import tnt.game.core.Fragment;
import tnt.game.storage.MessageEntry;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;
import android.widget.Toast;

public class DisplayFragment extends Fragment {
    public static final String ARGUMENT_MESSAGE = "ARGUMENT_MESSAGE";
    private static MessageEntry msg;
    
    public MessageActivity activity() {
        return (MessageActivity) getActivity();
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.messages_fragment_display, container, false);
        loading(v, true);
        msg = null;
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        
        MessageEntry selectedMessage = (MessageEntry) getArguments().getSerializable(ARGUMENT_MESSAGE);
        if (selectedMessage.getContact() == null || selectedMessage.getContent() == null) {
            new AsyncTask<MessageEntry, Void, MessageEntry>() {
                @Override
                protected MessageEntry doInBackground(MessageEntry... params) {
                    MessageEntry entry = params[0];
                    Messages api = container().api().getUserAPI(activity().user()).getMessagesAPI();
                    try {
                        Message message = api.readMessage(entry);
                        entry.updateFullMessage(message);
                        entry.setUnread(false);
                        container().db().update(entry);
                        container().db().close();
                        return entry;
                    } catch (ConnectionException e) {
                        return null;
                    }
                }
    
                @Override
                protected void onPostExecute(MessageEntry result) {
                    if (result == null) {
                        Toast.makeText(getActivity(), getString(R.string.error_connection_error), Toast.LENGTH_SHORT).show();
                        getFragmentManager().popBackStack();
                    } else {
                        displayMessage(result);
                        activity().updateList(result.getType(), false);
                    }
                }
            }.execute(selectedMessage);
        } else {
            displayMessage(selectedMessage);
        }
    }

    private void displayMessage(MessageEntry result) {
        msg = result;
        getActivity().supportInvalidateOptionsMenu();
        if (getView() != null) {
            ((TextView) getView().findViewById(R.id.message_from)).setText(result.getContact());
            ((TextView) getView().findViewById(R.id.message_subject)).setText(result.getSubject());
            if (result.getDate() != null)
                ((TextView) getView().findViewById(R.id.message_date)).setText(DateFormat.getDateTimeInstance().format(result.getDate()));
            else
                ((TextView) getView().findViewById(R.id.message_date)).setText("");
            StringBuilder html = new StringBuilder();
            html.append("<html><style type=\"text/css\">" +
                    "body {padding: 0; margin: 0; color: #cccccc; background: transparent; }" +
                    "</style><body>");
            html.append(result.getContent());
            html.append("</body></html>");
            final WebView msgs = (WebView) getView().findViewById(R.id.message_content);
            msgs.loadDataWithBaseURL(null, html.toString(), "text/html", null, null);
            msgs.setBackgroundColor(Color.TRANSPARENT);
            loading(getView(), false);
        }
    }
    
    protected void loading(View v, boolean show) {
        if (show) {
            v.findViewById(R.id.loading_spinner).setVisibility(View.VISIBLE);
            v.findViewById(R.id.content).setVisibility(View.GONE);
        } else {
            v.findViewById(R.id.loading_spinner).setVisibility(View.GONE);
            v.findViewById(R.id.content).setVisibility(View.VISIBLE);
        }
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();
        msg = null;
        getActivity().supportInvalidateOptionsMenu();
    }

    public MessageEntry getMessage() {
        return msg;
    }
}
