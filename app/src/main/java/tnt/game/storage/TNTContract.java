/*******************************************************************************
 * Copyright 2014 Jascha Starke <jascha@ja-s.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package tnt.game.storage;

import android.database.Cursor;
import de.jaschastarke.android.db.DBTable;

public final class TNTContract {
    private TNTContract() {}
    public static UserTable USER = new UserTable();
    public static ActivityTable ACTIVITY = new ActivityTable();
    public static MessageTable MESSAGE = new MessageTable();
    public static ItemTable ITEM = new ItemTable();
    
    public static final class UserTable implements DBTable<UserEntry> {
        private UserTable() {}
        
        public static final String TABLE_NAME = "user_entry";
        public static final String COLUMN_NAME_USERNAME = "username";
        public static final String COLUMN_NAME_HASH = "hash";
        public static final String COLUMN_NAME_USER_IMG = "user_img";
        public static final String COLUMN_NAME_GUARD_DIFFICULTY = "guard_difficulty";
        public static final String[] COLUMNS = new String[]{
            _ID, COLUMN_NAME_HASH, COLUMN_NAME_USERNAME, COLUMN_NAME_USER_IMG, COLUMN_NAME_GUARD_DIFFICULTY};
        
        public String getName() {
            return TABLE_NAME;
        }
        public UserEntry createEntry(Cursor cursor) {
            return new UserEntry(cursor);
        }
        public String[] getColumns() {
            return COLUMNS;
        }
    }
    
    public static final class ActivityTable implements DBTable<ActivityEntry> {
        private ActivityTable() {}
        
        public static final String TABLE_NAME = "activity_entry";
        public static final String COLUMN_NAME_USER_ID = "user_id";
        public static final String COLUMN_NAME_TYPE = "type";
        public static final String COLUMN_NAME_DURATION = "duration";
        public static final String COLUMN_NAME_TIME_END = "time_end";
        public static final String COLUMN_NAME_TITLE = "name";
        public static final String COLUMN_NAME_NOTIFIED = "notified";
        public static final String[] COLUMNS = new String[]{
            _ID, COLUMN_NAME_USER_ID, COLUMN_NAME_TYPE, COLUMN_NAME_DURATION, COLUMN_NAME_TIME_END,
            COLUMN_NAME_TITLE, COLUMN_NAME_NOTIFIED};

        @Override
        public String getName() {
            return TABLE_NAME;
        }
        @Override
        public ActivityEntry createEntry(Cursor cursor) {
            return new ActivityEntry(cursor);
        }
        @Override
        public String[] getColumns() {
            return COLUMNS;
        }
    }
    
    public static final class MessageTable implements DBTable<MessageEntry> {
        private MessageTable() {}
        
        public static final String TABLE_NAME = "message_entry";
        public static final String COLUMN_NAME_USER_ID = "user_id";
        public static final String COLUMN_NAME_MESSAGE_ID = "message_id";
        public static final String COLUMN_NAME_TYPE = "type"; // in or out
        public static final String COLUMN_NAME_DATE = "date";
        public static final String COLUMN_NAME_SUBJECT = "subject";
        public static final String COLUMN_NAME_CONTACT = "contact";
        public static final String COLUMN_NAME_CONTENT = "content";
        public static final String COLUMN_NAME_IS_UNREAD = "is_unread";
        public static final String[] COLUMNS = new String[]{
            _ID, COLUMN_NAME_USER_ID, COLUMN_NAME_MESSAGE_ID, COLUMN_NAME_TYPE, COLUMN_NAME_DATE,
            COLUMN_NAME_SUBJECT, COLUMN_NAME_CONTACT, COLUMN_NAME_CONTENT, COLUMN_NAME_IS_UNREAD};

        @Override
        public String getName() {
            return TABLE_NAME;
        }
        @Override
        public MessageEntry createEntry(Cursor cursor) {
            return new MessageEntry(cursor);
        }

        @Override
        public String[] getColumns() {
            return COLUMNS;
        }
        
    }
    
    public static final class ItemTable implements DBTable<ItemEntry> {
        public static final String TABLE_NAME = "item_entry";
        public static final String COLUMN_NAME_USER_ID = "user_id";
        //public static final String COLUMN_NAME_ITEM_ID = "item_id";
        public static final String COLUMN_NAME_TYPE = "type"; // inv / equip / treasure
        public static final String COLUMN_NAME_CATEGORY = "category";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_ICON = "icon";
        public static final String COLUMN_NAME_ATTACK = "attack";
        public static final String COLUMN_NAME_DEFENSE = "defense";
        public static final String COLUMN_NAME_LEVEL = "level";
        public static final String COLUMN_NAME_BONUSES = "bonuses";
        public static final String COLUMN_NAME_SORTING = "sorting";
        public static final String COLUMN_NAME_RESTRICTION = "restriction";
        public static final String[] COLUMNS = new String[]{
            _ID, COLUMN_NAME_USER_ID, COLUMN_NAME_TYPE, COLUMN_NAME_CATEGORY, COLUMN_NAME_NAME, COLUMN_NAME_ICON,
            COLUMN_NAME_ATTACK, COLUMN_NAME_DEFENSE, COLUMN_NAME_LEVEL, COLUMN_NAME_BONUSES, COLUMN_NAME_SORTING,
            COLUMN_NAME_RESTRICTION};
        
        @Override
        public String getName() {
            return TABLE_NAME;
        }
        @Override
        public ItemEntry createEntry(Cursor cursor) {
            return new ItemEntry(cursor);
        }
        @Override
        public String[] getColumns() {
            return COLUMNS;
        }
    }
}
