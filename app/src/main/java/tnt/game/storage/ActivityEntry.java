/*******************************************************************************
 * Copyright 2014 Jascha Starke <jascha@ja-s.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package tnt.game.storage;

import tnt.game.storage.TNTContract.ActivityTable;
import android.database.Cursor;
import de.jaschastarke.android.db.AbstractStringEntry;

public class ActivityEntry extends AbstractStringEntry {
    private static final long serialVersionUID = -322597976691830978L;
    
    public enum Type {
        MISSION(0),
        HUNT(1),
        BOSSBATTLE(2);
        private int value;
        Type(int val) {
            this.value = val;
        }
        public int value() {
            return value;
        }
        public static Type byValue(int v) {
            for (Type t : values()) {
                if (t.value() == v)
                    return t;
            }
            throw new IllegalArgumentException("Unknown Type: " + v);
        }
    }
    
    public ActivityEntry(Type t) {
        super();
        set(ActivityTable.COLUMN_NAME_TYPE, String.valueOf(t.value()));
        setBool(ActivityTable.COLUMN_NAME_NOTIFIED, false);
    }
    public ActivityEntry(Cursor c) {
        super(c);
    }
    public ActivityTable getTable() {
        return TNTContract.ACTIVITY;
    }
    public Type getType() {
        return Type.byValue(getInt(ActivityTable.COLUMN_NAME_TYPE));
    }
    public int getEnd() {
        return Integer.parseInt(get(ActivityTable.COLUMN_NAME_TIME_END));
    }
    public int getDuration() {
        return Integer.parseInt(get(ActivityTable.COLUMN_NAME_DURATION));
    }
    @Override
    public String toString() {
        return "MissionEntry{end=" + getEnd() + ", duration=" + getDuration() + "}";
    }
    public void setUser(UserEntry user) {
        setUser(user.getId());
    }
    public void setUser(long id) {
        set(ActivityTable.COLUMN_NAME_USER_ID, String.valueOf(id));
    }
    public long getUserId() {
        return Long.valueOf(get(ActivityTable.COLUMN_NAME_USER_ID));
    }
    public boolean isOutdated() {
        return (getEnd() * 1000L) < System.currentTimeMillis();
    }
    public void setTimer(int endTimestampSeconds, int durationSeconds) {
        set(ActivityTable.COLUMN_NAME_TIME_END, String.valueOf(endTimestampSeconds));
        set(ActivityTable.COLUMN_NAME_DURATION, String.valueOf(durationSeconds));
    }
    public void flagNotified() {
        setBool(ActivityTable.COLUMN_NAME_NOTIFIED, true);
    }
    public boolean isNotified() {
        return getBool(ActivityTable.COLUMN_NAME_NOTIFIED);
    }
    public void setName(String name) {
        set(ActivityTable.COLUMN_NAME_TITLE, name);
    }
    public String getName() {
        return getNotEmpty(ActivityTable.COLUMN_NAME_TITLE);
    }
}
