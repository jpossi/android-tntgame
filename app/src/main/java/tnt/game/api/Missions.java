/*******************************************************************************
 * Copyright 2014 Jascha Starke <jascha@ja-s.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package tnt.game.api;

import java.io.Serializable;
import java.util.List;

public interface Missions {
    public interface MissionHeader extends Serializable {
        /*public int getPosition();*/
        public String getTitle();
        public String getDesc();
    }
    
    public interface MissionDetail extends MissionHeader {
        public int getEndurance();
        public String getDuration();
        public int getExp();
        public int getGold();
    }
    
    public enum State {
        MISSION_CHOICE,
        IN_WORK,
        HUNT_ACTIVE, // Hunt is also in work, but shouldn't be handled by Mission-API
        HUNT_FINISHED, // In work finished
        BATTLE_REPORT,
        IN_BOSSBATTLE;
    }
    
    /**
     * Executes an update of the current state, and catches the data associated with the new state.
     * Doing this may affect the state, as the battle report isn't a permanent state and vanishes due to fetching.
     * 
     * Uses network. To be used asynchronous.
     */
    public State fetchCurrentState() throws ConnectionException;
    /**
     * Requires fetchCurrentState() first
     * 
     * @return null if currentState is not IN_WORK
     */
    public Integer getTimer();

    /**
     * Requires fetchCurrentState() first
     * 
     * @return null if currentState is not MISSION_CHOICE
     */
    public List<MissionHeader> getMissions();

    /**
     * Requires fetchCurrentState() first
     * 
     * @return null if currentState is not BATTLE_REPORT
     */
    public BattleReport getBattleReport();

    /**
     * Uses network. To be used asynchronous.
     */
    public MissionDetail getMission(MissionHeader mission) throws ConnectionException;
    
    /**
     * The Mission-Parameter may be ignored, depending on the API-Implementation, then the latest getMission() is used.
     * @throws SessionTimeoutException If the session timed out after selection the Mission via getMission()
     * @return false if mission couldn't be started (due to missing endurance)
     */
    public boolean startMission(MissionDetail mission) throws ConnectionException, SessionTimeoutException;
}
