/*******************************************************************************
 * Copyright 2014 Jascha Starke <jascha@ja-s.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package tnt.game.api;

import java.io.Serializable;
import java.util.List;

public interface Inventory {
    public enum Type {
        INVENTORY(0),
        EQUIPMENT(1),
        TREASURE(2);
        private int val;
        Type(int val) {
            this.val = val;
        }
        public int value() {
            return val;
        }
        public static Type byValue(int val) {
            for (Type v : Type.values())
                if (v.value() == val)
                    return v;
            throw new IllegalArgumentException();
        }
    }
    
    public enum Category {
        NORMAL(null),
        RUNES("#003300"),
        SPECIAL("#993399"),
        PREMIUM("#800000"),
        EPIC("purple"), // Purple
        UNIQUE("#009999"),
        RARE_UNIQUE("#CCCC00");
        private String color;
        Category(String htmlColor) {
            this.color = htmlColor;
        }
        public String getOriginalColor() {
            return color;
        }
    }
    
    public interface Item extends Serializable {
        public int getItemId();
        public Type getType();
        public String getIcon();
        public String getName();
        public Category getCategory();
        public int getAttack();
        public int getDefense();
        public int getLevel();
        public List<Bonus> getBonuses();
        public String getRestriction();
        
        public interface Bonus extends Serializable {
            /**
             * @return HTML
             */
            public String getLabel();
        }
    }

    /**
     * Uses network. To be used asynchronous.
     */
    public List<Item> fetchInventory(Type type) throws ConnectionException;

    /**
     * Requires fetchInventory(INVENTORY) first
     * 
     * Uses network. To be used asynchronous.
     * @param item of the Type INVENTORY
     * @throws SessionTimeoutException If the session timed out after selection the Inventory via fetchInventory()
     */
    public boolean equipItem(Item item) throws ConnectionException, SessionTimeoutException;

    /**
     * Requires fetchInventory(EQUIPMENT) first
     * 
     * Uses network. To be used asynchronous.
     * @param item of the Type EQUIPMENT
     * @throws SessionTimeoutException If the session timed out after selection the Inventory via fetchInventory()
     */
    public boolean unequipItem(Item item) throws ConnectionException, SessionTimeoutException;

    /**
     * Requires fetchInventory(EQUIPMENT) first
     * 
     * Uses network. To be used asynchronous.
     * @throws SessionTimeoutException If the session timed out after selection the Inventory via fetchInventory()
     */
    public boolean unequipEverything() throws ConnectionException, SessionTimeoutException;
}
