/*******************************************************************************
 * Copyright 2014 Jascha Starke <jascha@ja-s.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package tnt.game.api;

public interface Guard {
    public enum Difficulty {
        VERY_SIMPLE(0),
        SIMPLE(1),
        MEDIUM(2),
        HARD(3),
        VERY_HARD(4);
        
        private int val;
        private Difficulty(int v) {
            this.val = v;
        }
        public int value() {
            return val;
        }
        public static Difficulty byValue(int v) {
            for (Difficulty e : Difficulty.values()) {
                if (e.value() == v)
                    return e;
            }
            //return null;
            throw new IllegalArgumentException("No Difficulty with value " + v + " found");
        }
    }
    /**
     * Uses network. To be used asynchronous.
     */
    public int getFreeCount() throws ConnectionException;
    /**
     * Requires getFreeCount() first
     */
    public int getRegenTimeleft();
    /**
     * Requires getFreeCount() first
     * @throws SessionTimeoutException If the session timed out after selection the Mission via getMission()
     */
    public BattleReport doGuard(Difficulty difficulty) throws ConnectionException, SessionTimeoutException;

    /**
     * Requires getFreeCount() first
     * @throws SessionTimeoutException If the session timed out after selection the Mission via getMission()
     */
    public BattleReport doAllGuard(Difficulty difficulty) throws ConnectionException, SessionTimeoutException;
    /**
     * Like doGuard(), but skips the BattleReport for performance reason.
     * 
     * Requires getFreeCount() first
     * @throws SessionTimeoutException If the session timed out after selection the Mission via getMission()
     */
    public void doSilentGuard(Difficulty difficulty) throws ConnectionException, SessionTimeoutException;
}
