/*******************************************************************************
 * Copyright 2014 Jascha Starke <jascha@ja-s.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package tnt.game.api;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public interface Messages {
    public interface MessageHeader extends Serializable {
        public int getMessageId();
        public Date getDate();
        public String getSubject();
        public boolean isUnread();
        public String getContact();
    }
    public interface Message extends MessageHeader {
        public String getContact();
        public String getContent();
    }
    public interface NewMessage {
        public String getRecipient();
        public String getSubject();
        public String getContent();
    }
    
    public enum BoxType {
        INBOX,
        OUTBOX,
        INFORMATION;
        
        public static BoxType get(int ordinal) {
            for (BoxType v : values())
                if (v.ordinal() == ordinal)
                    return v;
            throw new IllegalArgumentException();
        }
    }


    /**
     * Uses network. To be used asynchronous.
     */
    public boolean fetchBoxes() throws ConnectionException;
    /**
     * Requires fetchBoxes first.
     */
    public List<MessageHeader> getBox(BoxType type);
    /**
     * Uses network. To be used asynchronous.
     */
    public boolean deleteAll(BoxType type) throws ConnectionException, SessionTimeoutException;
    /**
     * Uses network. To be used asynchronous.
     */
    public Message readMessage(MessageHeader msg) throws ConnectionException;
    /**
     * Uses network. To be used asynchronous.
     */
    public boolean deleteMessage(Message msg) throws ConnectionException;
    /**
     * Uses network. To be used asynchronous.
     */
    public boolean sendMessage(NewMessage msg) throws ConnectionException;
}
