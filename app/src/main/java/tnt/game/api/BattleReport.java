/*******************************************************************************
 * Copyright 2014 Jascha Starke <jascha@ja-s.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package tnt.game.api;

import java.io.Serializable;
import java.util.List;

public interface BattleReport extends Serializable {
    public enum Winner {
        ATTACKER,
        DEFENDER;
    }
    
    public boolean isWon();
    public Winner getWinner();
    public String getWinnerName();
    public int getGold();
    public int getExp();
    public String getAttacker();
    public String getDefener();
    public List<String> getLog();
    public String getOpponentImage();
}
