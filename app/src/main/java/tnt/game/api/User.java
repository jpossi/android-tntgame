/*******************************************************************************
 * Copyright 2014 Jascha Starke <jascha@ja-s.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package tnt.game.api;

import java.io.File;
import java.util.Date;

public interface User {

    public interface Session {
        public String getSessionId();
        public Date getSessionTimestamp();
    }
    public Session getSession();
    
    public enum Stats {
        MAIN,
        DEFENSE,
        LIFE,
        LUCK,
        UNUSED;
    }
    
    public enum MainStat {
        INTELLIGENCE,
        STRENGTH,
        AGILITY,
        ATTACK
    }
    
    public enum Money {
        GOLD,
        RELICTS,
        DIAS
    }
    
    public Missions getMissionsAPI();
    public Guard getGuardAPI();
    public Hunt getHuntAPI();
    public BossBattle getBossAPI();
    public Messages getMessagesAPI();
    public Inventory getInventoryAPI();
    
    public int getMoney(Money m);
    public int getStat(Stats m);
    public String getStatValue(Stats luck);
    public MainStat getMainStat();
    public int getLevel();
    
    public String getDisplayName();
    
    /**
     * @TODO Split Points and Rank
     */
    public String getRank();
    /**
     * @TODO Strip "Ausdauer"
     */
    public String getEndurance();
    /**
     * @TODO Strip "EXP"
     */
    public String getExpLeft();

    public boolean cacheImage(File file);

    /**
     * @return the basename of the image of that user. is used to determine if the image has been changed
     */
    public String getImage();

    /**
     * Uses network. To be used asynchronous.
     */
    public void refresh() throws ConnectionException;
    
    public boolean hasNewMessages();
    
    public int getPrice(Stats m);
    public int doStatInc(Stats m);
}
