/*******************************************************************************
 * Copyright 2014 Jascha Starke <jascha@ja-s.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package tnt.game.status;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Collection;
import java.util.Iterator;
import java.util.Locale;

import com.actionbarsherlock.view.ActionProvider;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.SubMenu;

import tnt.game.R;
import tnt.game.StatusActivity;
import tnt.game.core.Activity;
import tnt.game.core.Container;
import tnt.game.storage.UserEntry;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

public class SelectCharActionProvider extends ActionProvider {
    public static final int ITEM_GROUP_ID = 0xff871202;
    public static final int ITEM_ID_ADD = 0xff871203;
    private Activity context;
    private Collection<UserEntry> userList;

    public SelectCharActionProvider(StatusActivity context, Collection<UserEntry> set) {
        super(context);
        this.context = context;
        this.userList = set;
    }
    
    public Container container() {
        return Container.getForContext(context);
    }
    
    @Override
    public void onPrepareSubMenu(SubMenu subMenu) {
        subMenu.clear();
        
        int i = 0;
        for (Iterator<UserEntry> iterator = userList.iterator(); iterator.hasNext(); i++) {
            final UserEntry user = iterator.next();
            if (user.getId() == 999){ break; }
            final MenuItem item = subMenu.add(ITEM_GROUP_ID, (int) user.getId(), i, user.getName());
            if (user.getId() == context.user().getId()) {
                item.setEnabled(false);
                item.setIcon(subMenu.getItem().getIcon());
            } else {
                new AsyncTask<Void, Void, BitmapDrawable>() {
                    @Override
                    protected BitmapDrawable doInBackground(Void... params) {
                        File file = new File(context.getFilesDir(), String.format(Locale.US, "icon_char_%d.png", user.getId()));
                        if (file.exists()) {
                            try {
                                Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(file));
                                return new BitmapDrawable(context.getResources(), bitmap);
                            } catch (FileNotFoundException e) {
                                Log.e("tnt.game", "Exception: couldn't read " + file, e);
                            }
                        }
                        return null;
                    }
                    @Override
                    protected void onPostExecute(BitmapDrawable result) {
                        if (result != null) {
                            item.setIcon(result);
                        }
                    }
                }.execute();
            }
        }
        
        MenuItem add = subMenu.add(1, ITEM_ID_ADD, 999, R.string.add_login);
        add.setIcon(R.drawable.ic_action_new);
    }
    @Override
    public boolean hasSubMenu() {
        return true;
    }
    @Override
    public View onCreateActionView() {
        return null;
    }
    public void setActionProviderTo(final MenuItem menuItem) {
        menuItem.setActionProvider(this);
        Drawable icon = context.container().state().getUserIcon(context.user());
        if (icon != null) {
            menuItem.setIcon(icon);
        } else {
            menuItem.setIcon(context.getResources().getDrawable(R.drawable.ic_action_new));
        }
        menuItem.setVisible(true);
        /*new AsyncTask<Void, Void, BitmapDrawable>() {
            @Override
            protected BitmapDrawable doInBackground(Void... params) {
                File file = context.getIconImageFile(); // long running operation
                if (file.exists()) {
                    try {
                        Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(file));
                        return new BitmapDrawable(context.getResources(), bitmap);
                    } catch (FileNotFoundException e) {
                        Log.e("tnt.game", "Exception: couldn't read " + file, e);
                    }
                }
                return null;
            }
            @Override
            protected void onPostExecute(BitmapDrawable result) {
                if (result != null) {
                    menuItem.setIcon(result);
                } else {
                    menuItem.setIcon(context.getResources().getDrawable(R.drawable.ic_action_new));
                }
                menuItem.setVisible(true);
            }
        }.execute();*/
    }
    
    /*class Adapter extends SimpleAdapter {
        public Adapter(Context context, List<? extends Map<String, ?>> data, int resource, String[] from, int[] to) {
            super(context, data, resource, from, to);
        }
        
    }*/

    /*@Override
    @SuppressWarnings("deprecation")
    public View onCreateActionView() {
        /*ImageView view = new ImageView(context);
        view.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_action_new));
        view.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.spinner_ab_default_holo_dark));
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SelectCharActionProvider.this.
            }
        });* /
        
        final Spinner spinner = new Spinner(context);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                T ODO Auto-generated method stub
                
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });
        
        
        
        return spinner;
    }*/
}
