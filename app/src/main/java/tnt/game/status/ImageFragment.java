/*******************************************************************************
 * Copyright 2014 Jascha Starke <jascha@ja-s.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package tnt.game.status;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import tnt.game.R;
import tnt.game.StatusActivity;
import tnt.game.core.Fragment;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

public class ImageFragment extends Fragment {
    private AsyncTask<File, Void, Drawable> fetchTask;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.status_fragment_image, container, false);
    }
    
    @Override
    public void onStart() {
        super.onStart();
        draw();
    }

    public void draw() {
        final ImageView imageView = (ImageView) getView().findViewById(R.id.char_image);
        fetchTask = new AsyncTask<File, Void, Drawable>(){
            @Override
            protected Drawable doInBackground(File... params) {
                try {
                    if (getActivity() == null)
                        return null;
                    File file = ((StatusActivity) getActivity()).getImageFile(); // long running operation
                    if (file == null)
                        return null;
                    Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(file));
                    return new BitmapDrawable(getResources(), bitmap);
                } catch (FileNotFoundException e) {
                    Log.e("tnt.game", "Exception: Couldn't access char.png", e);
                }
                return null;
            }
            @Override
            protected void onPostExecute(Drawable result) {
                if (getView() != null && getActivity() != null) {
                    if (result != null) {
                        if (getView() != null && getView().findViewById(R.id.loading_char_image) != null) {
                            getView().findViewById(R.id.loading_char_image).setVisibility(View.GONE);
                            imageView.setImageDrawable(result);
                            imageView.setVisibility(View.VISIBLE);
                        }
                    } else {
                        Toast.makeText(getActivity(), getString(R.string.error_load_charimage), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }.execute();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (fetchTask != null)
            fetchTask.cancel(false);
    }
}
