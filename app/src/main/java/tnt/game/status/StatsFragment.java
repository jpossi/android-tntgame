/*******************************************************************************
 * Copyright 2014 Jascha Starke <jascha@ja-s.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package tnt.game.status;

import com.tntgame.api.AspxUser;

import tnt.game.R;
import tnt.game.StatusActivity;
import tnt.game.api.User;
import tnt.game.core.Fragment;
import tnt.game.storage.UserEntry;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class StatsFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.status_fragment_stats, container, false);
    }
    
    @Override
    public void onStart() {
        super.onStart();
        draw();
    }

    public void draw() {
        UserEntry user = ((StatusActivity) getActivity()).user();
        User api = container().api().getUserAPI(user);
        View v = getView();
        
        ((TextView) v.findViewById(R.id.status_name)).setText(api.getDisplayName());
        ((TextView) v.findViewById(R.id.status_level)).setText(String.valueOf(api.getLevel()));
        ((TextView) v.findViewById(R.id.status_exp_left)).setText(api.getExpLeft());
        ((TextView) v.findViewById(R.id.status_gold)).setText(String.valueOf(api.getMoney(AspxUser.Money.GOLD)));
        ((TextView) v.findViewById(R.id.status_relicts)).setText(String.valueOf(api.getMoney(AspxUser.Money.RELICTS)));
        ((TextView) v.findViewById(R.id.status_dias)).setText(String.valueOf(api.getMoney(AspxUser.Money.DIAS)));
        
        ((TextView) v.findViewById(R.id.status_endurance)).setText(api.getEndurance());
        ((TextView) v.findViewById(R.id.status_rank)).setText(api.getRank());

        User.MainStat stat = api.getMainStat();
        if (stat != null) {
            switch (stat) {
                case INTELLIGENCE:
                    ((TextView) v.findViewById(R.id.status_main_label)).setText(getString(R.string.status_int));
                    break;
                case STRENGTH:
                    ((TextView) v.findViewById(R.id.status_main_label)).setText(getString(R.string.status_strength));
                    break;
                case AGILITY:
                    ((TextView) v.findViewById(R.id.status_main_label)).setText(getString(R.string.status_agility));
                    break;
                default:
                    ((TextView) v.findViewById(R.id.status_main_label)).setText(getString(R.string.status_attack));
                    break;
            }
        }
        ((TextView) v.findViewById(R.id.status_main_attr)).setText(api.getStatValue(User.Stats.MAIN));
        ((TextView) v.findViewById(R.id.status_def)).setText(api.getStatValue(User.Stats.DEFENSE));
        ((TextView) v.findViewById(R.id.status_life)).setText(api.getStatValue(User.Stats.LIFE));
        ((TextView) v.findViewById(R.id.status_luck)).setText(api.getStatValue(User.Stats.LUCK));
        ((TextView) v.findViewById(R.id.status_skill_points)).setText(api.getStatValue(User.Stats.UNUSED));
        
        /*if (api.getStat(User.Stats.UNUSED) != 0) {
            String fprice = getString(R.string.plus_price);

            ((Button) v.findViewById(R.id.status_strength_price)).setText(String.format(fprice, api.getPrice(User.Stats.MAIN)));
            ((Button) v.findViewById(R.id.status_strength_price)).setVisibility(View.VISIBLE);

            ((Button) v.findViewById(R.id.status_defense_price)).setText(String.format(fprice, api.getPrice(User.Stats.DEFENSE)));
            ((Button) v.findViewById(R.id.status_defense_price)).setVisibility(View.VISIBLE);

            ((Button) v.findViewById(R.id.status_life_price)).setText(String.format(fprice, api.getPrice(User.Stats.LIFE)));
            ((Button) v.findViewById(R.id.status_life_price)).setVisibility(View.VISIBLE);

            ((Button) v.findViewById(R.id.status_luck_price)).setText(String.format(fprice, api.getPrice(User.Stats.LUCK)));
            ((Button) v.findViewById(R.id.status_luck_price)).setVisibility(View.VISIBLE);
        } else {*/
            ((Button) v.findViewById(R.id.status_strength_price)).setVisibility(View.GONE);
            ((Button) v.findViewById(R.id.status_defense_price)).setVisibility(View.GONE);
            ((Button) v.findViewById(R.id.status_life_price)).setVisibility(View.GONE);
            ((Button) v.findViewById(R.id.status_luck_price)).setVisibility(View.GONE);
        //}
    }
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (getActivity() != null) {// failsafe for first launch
            ((StatusActivity) getActivity()).setDrawerSwipe(isVisibleToUser);
        }
    }
}
