/*******************************************************************************
 * Copyright 2014 Jascha Starke <jascha@ja-s.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package tnt.game.status;

import java.util.Locale;

import tnt.game.HuntActivity;
import tnt.game.MissionsActivity;
import tnt.game.R;
import tnt.game.StatusActivity;
import tnt.game.core.Activity;
import tnt.game.core.Fragment;
import tnt.game.storage.ActivityEntry;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

public class MainFragment extends Fragment {
    private ActivityEntry activity;
    private CountDownTimer timer;
    
    protected StatusActivity act() {
        return (StatusActivity) getActivity();
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.status_fragment_menu, container, false);
    }
    
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getHolder().notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (activity.getType()) {
                    case HUNT:
                        Intent hIntent = new Intent(getActivity(), HuntActivity.class);
                        hIntent.putExtra(Activity.EXTRA_USER_ENTRY, act().user());
                        startActivity(hIntent);
                        break;
                    case MISSION:
                        Intent mIntent = new Intent(getActivity(), MissionsActivity.class);
                        mIntent.putExtra(Activity.EXTRA_USER_ENTRY, act().user());
                        startActivity(mIntent);
                        break;
                    case BOSSBATTLE:
                        break;
                }
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        new AsyncTask<Void, Void, ActivityEntry>() {
            @Override
            protected ActivityEntry doInBackground(Void... params) {
                synchronized (container().db()) {
                    if (getActivity() == null)
                        return null;
                    ActivityEntry activity = container().db().findActivity(act().user());
                    container().db().close();
                    return activity;
                }
            }
            @Override
            protected void onPostExecute(ActivityEntry result) {
                activity = result;
                if (getView() != null && getActivity() != null) {
                    if (activity != null)
                        updateStatus();
                    else
                        getView().findViewById(R.id.notification).setVisibility(View.GONE);
                }
            }
        }.execute();
    }
    
    @Override
    public void onStop() {
        super.onStop();
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    protected Holder getHolder() {
        if (getView().getTag() == null) {
            Holder h = new Holder();
            h.notification = getView().findViewById(R.id.notification);
            h.notification_text = (TextView) getView().findViewById(R.id.notification_text);
            h.notification_subtext = (TextView) getView().findViewById(R.id.notification_subtext);
            h.time_left = (TextView) getView().findViewById(R.id.time_left);
            h.progress = (ProgressBar) getView().findViewById(R.id.progress);
            getView().setTag(h);
            return h;
        } else {
            return (Holder) getView().getTag();
        }
    }
    
    private class Holder {
        TextView notification_text;
        TextView notification_subtext;
        TextView time_left;
        View notification;
        ProgressBar progress;
    }
    
    protected void hideStatus() {
        getHolder().notification.setVisibility(View.GONE);
    }
    
    protected void updateStatus() {
        String title = null;
        String hint = null;
        switch (activity.getType()) {
            case BOSSBATTLE:
                title = getString(R.string.status_in_bossbattle);
                break;
            case HUNT:
                title = getString(R.string.status_on_hunt);
                hint = activity.getName();
                break;
            case MISSION:
                title = getString(R.string.status_on_mission);
                hint = activity.getName();
                break;
        }
        
        int timeleft = activity.getEnd() - ((int) (System.currentTimeMillis() / 1000));
        
        Holder h = getHolder();
        
        h.notification_text.setText(title);
        h.notification_subtext.setText(hint);
        h.notification_subtext.setVisibility(hint != null ? View.VISIBLE : View.GONE);
        h.progress.setMax(activity.getDuration());
        h.progress.setProgress(activity.getDuration() - timeleft);
        
        h.notification.setVisibility(View.VISIBLE);
        h.notification.setFocusable(activity.getType() != ActivityEntry.Type.BOSSBATTLE);
        h.notification.setClickable(activity.getType() != ActivityEntry.Type.BOSSBATTLE);
        h.notification.setEnabled(true);
        
        showTimeLeft(timeleft);
        
        if (timeleft > 0) {
            timer = new CountDownTimer(timeleft * 1000, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    showTimeLeft((int) (millisUntilFinished / 1000));
                }
                @Override
                public void onFinish() {
                    showTimeLeft(0);
                }
            }.start();
        }
    }
    
    protected void showTimeLeft(int timeleft) {
        if (getView() == null)
            return;
        Holder hd = getHolder();
        int h = (int) Math.floor(timeleft / 3600);
        int m = (int) Math.floor((timeleft % 3600) / 60);
        int s = timeleft % 60;
        if (h > 0) {
            hd.time_left.setText(String.format(Locale.US, "%d:%02d:%02d", h, m, s));
        } else if (timeleft >= 0) {
            hd.time_left.setText(String.format(Locale.US, "%02d:%02d", m, s));
        } else {
            hd.time_left.setText("");
        }
        hd.progress.setProgress(hd.progress.getMax() - timeleft);
    }

}
