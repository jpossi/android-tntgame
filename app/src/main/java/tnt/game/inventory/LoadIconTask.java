/*******************************************************************************
 * Copyright 2014 Jascha Starke <jascha@ja-s.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package tnt.game.inventory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;

import tnt.game.R;
import tnt.game.api.ConnectionException;
import tnt.game.core.Container;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;
import de.jaschastarke.utils.IOUtil;

public class LoadIconTask extends AsyncTask<Void, Void, Bitmap> {
    private static final int TAG_CURRENT_ICON = 0x9fa269db;
    private static final int TAG_CURRENT_TASK = 0x9fa269dc;
    
    public interface Callback {
        public void OnLoadComplete(View view, Bitmap bitmap);
    }
    
    public static boolean loadIconInto(View view, String icon, Context context) {
        String currentIcon = (String) view.getTag(TAG_CURRENT_ICON);
        if (currentIcon == null || currentIcon != icon) {
            LoadIconTask task = (LoadIconTask) view.getTag(TAG_CURRENT_TASK);
            if (task != null && !task.isCancelled()) {
                if (!task.icon.equals(icon)) {
                    task.cancel(false);
                } else {
                    return false;
                }
            }
            task = new LoadIconTask(context, view, icon);
            task.execute();
            view.setTag(TAG_CURRENT_TASK, task);
            return true;
        } else {
            AsyncTask<?, ?, ?> cTask = (AsyncTask<?, ?, ?>) view.getTag(TAG_CURRENT_TASK);
            if (cTask != null)
                cTask.cancel(false);
        }
        return false;
    }
    
    public static void setCallback(View view, Callback callback) {
        LoadIconTask task = (LoadIconTask) view.getTag(TAG_CURRENT_TASK);
        if (task != null)
            task.callback = callback;
    }
    
    private String icon;
    private WeakReference<View> viewRef;
    private Context context;
    private Callback callback;
    
    public LoadIconTask(Context context, View view, String icon) {
        this.context = context;
        this.viewRef = new WeakReference<View>(view);
        this.icon = icon;
    }
    
    protected Container container() {
        return Container.getForContext(context);
    }
    
    @Override
    protected Bitmap doInBackground(Void... params) {
        String cacheFileName = icon.replace("/", "_");
        File file = new File(context.getCacheDir(), cacheFileName);
        
        if (!file.exists()) {
            InputStream stream;
            try {
                stream = container().api().loadImage(icon);
            } catch (ConnectionException e) {
                return null;
            }
            if (isCancelled())
                return null;
            
            try {
                IOUtil.copy(stream, new FileOutputStream(file));
            } catch (FileNotFoundException e) {
                Log.e("tnt.game", "Exception: Failed to write " + file, e);
                return null;
            } catch (IOException e) {
                Log.e("tnt.game", "Exception: Failed to write " + file, e);
                return null;
            }
            if (isCancelled())
                return null;
        }

        Bitmap bitmap;
        try {
            bitmap = BitmapFactory.decodeStream(new FileInputStream(file));
        } catch (FileNotFoundException e) {
            Log.e("tnt.game", "Exception: Failed to load " + file, e);
            return null;
        }
        if (isCancelled())
            return null;
        
        return bitmap;
    }

    @Override
    protected void onPostExecute(Bitmap result) {
        View view = viewRef.get();
        if (view != null) {
            if (result != null) {
                //view.setImageDrawable(result);
                if (view instanceof ImageView)
                    ((ImageView) view).setImageBitmap(result);
                view.setTag(TAG_CURRENT_ICON, icon);
                if (callback != null)
                    callback.OnLoadComplete(view, result);
            } else {
                Toast.makeText(context, context.getString(R.string.error_load_image), Toast.LENGTH_SHORT).show();
            }
            view.setTag(TAG_CURRENT_TASK, null);
        }
    }
}
