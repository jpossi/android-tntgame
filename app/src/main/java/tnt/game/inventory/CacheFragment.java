/*******************************************************************************
 * Copyright 2014 Jascha Starke <jascha@ja-s.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package tnt.game.inventory;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.util.LruCache;

public class CacheFragment extends Fragment {
    private static final String TAG = "RetainFragment";
    private static final double CACHESIZE_BITMAP_VM_FRACTION = 0.125;
    private LruCache<String, Bitmap> retainedBitmapCache;

    public CacheFragment() {}

    public static CacheFragment findOrCreateRetainFragment(FragmentManager fm) {
        CacheFragment fragment = (CacheFragment) fm.findFragmentByTag(TAG);
        if (fragment == null) {
            fragment = new CacheFragment();
            fm.beginTransaction().add(fragment, TAG).commit();
        }
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }
    
    public LruCache<String, Bitmap> getBitmapCache() {
        if (retainedBitmapCache == null) {
            final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
            final int cacheSize = (int) (maxMemory * CACHESIZE_BITMAP_VM_FRACTION);
            retainedBitmapCache = new LruCache<String, Bitmap>(cacheSize) {
                @Override
                @SuppressLint("NewApi")
                protected int sizeOf(String key, Bitmap bitmap) {
                    int size;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1) {
                        size = bitmap.getByteCount();
                    } else {
                        size = bitmap.getRowBytes() * bitmap.getHeight();
                    }
                    // The cache size will be measured in kilobytes rather than
                    // number of items.
                    return size / 1024;
                }
            };
        }
        return retainedBitmapCache;
    }
}
