/*******************************************************************************
 * Copyright 2014 Jascha Starke <jascha@ja-s.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package tnt.game.inventory;

import java.util.List;

import tnt.game.InventoryActivity;
import tnt.game.R;
import tnt.game.api.Inventory;
import tnt.game.api.Inventory.Item;
import tnt.game.api.Inventory.Type;
import tnt.game.core.Fragment;
import tnt.game.storage.ItemEntry;
import tnt.game.storage.TNTContract;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.support.v4.util.LruCache;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import de.jaschastarke.android.db.DBLoader;

public class ItemListFragment extends Fragment
        implements LoaderCallbacks<List<ItemEntry>> {
    public static final String ARG_INV_TYPE = "ARGUMENT_INVENTORY_TYPE";
    private static final String STATE_INITIAL_LOADED = "SAVECINSTANCESTATE_INITIAL_LOADED";
    private Adapter adapter;
    private LruCache<String, Bitmap> bitmapCache;
    private boolean initalLoaded = false;
    private Inventory.Type invtype = Type.INVENTORY;
    
    protected InventoryActivity activity() {
        return (InventoryActivity) getActivity();
    }
    
    protected Inventory api() {
        return activity().userApi().getInventoryAPI();
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        invtype = (Type) getArguments().getSerializable(ARG_INV_TYPE);
        
        return inflater.inflate(R.layout.inventory_fragment_items, null, false);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null)
            initalLoaded = savedInstanceState.getBoolean(STATE_INITIAL_LOADED);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(STATE_INITIAL_LOADED, initalLoaded);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        
        bitmapCache = activity().getCacheFragment().getBitmapCache();
        adapter = new Adapter();
        
        GridView grid = (GridView) getView().findViewById(R.id.grid);
        grid.setAdapter(adapter);
        grid.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ItemEntry item = adapter.getItem(position);
                TooltipFragment.showTooltip(item, getFragmentManager());
            }
        });
        grid.setEmptyView(getView().findViewById(android.R.id.empty));
        
        getLoaderManager().initLoader(invtype.value(), null, this);
    }

    public void onPageSelect() {
        if (!initalLoaded) {
            update(true);
            initalLoaded = true;
        }
    }
    
    public void update(boolean fullUpdate) {
        if (fullUpdate) {
            new UpdateInventoryTask(activity()) {
                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    activity().indicateLoading(true);
                }
                @Override
                protected void onPostExecute(Boolean result) {
                    if (getActivity() != null) {
                        super.onPostExecute(result);
                        activity().indicateLoading(false);
                        if (result) {
                            getLoaderManager().restartLoader(invtype.value(), null, ItemListFragment.this);
                        }
                    }
                }
            }.execute(invtype);
        } else {
            getLoaderManager().restartLoader(invtype.value(), null, ItemListFragment.this);
        }
    }

    class Adapter extends BaseAdapter {
        private List<ItemEntry> items;

        public void replaceData(List<ItemEntry> data) {
            this.items = data;
            notifyDataSetChanged();
        }
        public List<ItemEntry> getData() {
            return items;
        }
        @Override
        public int getCount() {
            if (items == null)
                return 0;
            return items.size();
        }
        @Override
        public ItemEntry getItem(int position) {
            return items.get(position);
        }
        @Override
        public long getItemId(int position) {
            return items.get(position).getItemId();
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.inventory_grid_item, parent, false);
            }
            
            //Log.d("tnt.game", "Adapter getView: " + position);
            final Item item = getItem(position);
            ImageView icon = (ImageView) convertView.findViewById(R.id.icon);
            
            Bitmap img = bitmapCache.get(item.getIcon());
            if (img != null) {
                //Log.d("tnt.game", "Loading " + item.getIcon() + " from LruCache");
                icon.setImageBitmap(img);
            } else if (LoadIconTask.loadIconInto(icon, item.getIcon(), getActivity())) {
                //Log.d("tnt.game", "Loading " + item.getIcon() + " from Disk/Network");
                //icon.setImageDrawable(getResources().getDrawable(R.drawable.abs__progress_medium_holo));
                icon.setImageDrawable(null);
                LoadIconTask.setCallback(icon, new LoadIconTask.Callback() {
                    @Override
                    public void OnLoadComplete(View view, Bitmap bitmap) {
                        bitmapCache.put(item.getIcon(), bitmap);
                    }
                });
            } else {
                //Log.d("tnt.game", "Icon " + item.getIcon() + " alread set");
            }
            
            return convertView;
        }
    }
    
    @Override
    public Loader<List<ItemEntry>> onCreateLoader(int id, Bundle args) {
        if (Type.byValue(id) == null)
            throw new IllegalArgumentException();
        return new DBLoader<ItemEntry>(getActivity(), activity().container().db(), TNTContract.ITEM)
                .setWhere(TNTContract.ItemTable.COLUMN_NAME_TYPE + " = ? AND " +
                        TNTContract.ItemTable.COLUMN_NAME_USER_ID + " = ?")
                .setWhereArgs(new String[] {String.valueOf(id), String.valueOf(activity().user().getId())})
                .setOrderBy(TNTContract.ItemTable.COLUMN_NAME_SORTING + " ASC");
    }
    @Override
    public void onLoadFinished(Loader<List<ItemEntry>> loader, List<ItemEntry> data) {
        adapter.replaceData(data);
    }
    @Override
    public void onLoaderReset(Loader<List<ItemEntry>> loader) {
        adapter.replaceData(null);
    }
}
