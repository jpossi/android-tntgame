/*******************************************************************************
 * Copyright 2014 Jascha Starke <jascha@ja-s.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package tnt.game.inventory;

import java.util.List;

import tnt.game.InventoryActivity;
import tnt.game.R;
import tnt.game.api.ConnectionException;
import tnt.game.api.Inventory;
import tnt.game.api.Inventory.Item.Bonus;
import tnt.game.api.Inventory.Type;
import tnt.game.api.SessionTimeoutException;
import tnt.game.storage.ItemEntry;
import tnt.game.storage.TNTDBHelper;
import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.util.LruCache;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class TooltipFragment extends DialogFragment {
    private static final String ARG_ITEM_ENTRY = "ARGUMENT_SERIALIZED_ITEM_ENTRY";
    private static final String BACKSTACK_TOOLTIP = "BACKSTACK_TOOLTIP";
    private static final String TAG = "tnt.game.inventory.TooltipFragment";
    
    public static TooltipFragment showTooltip(ItemEntry item, FragmentManager fragmentManager) {
        FragmentTransaction ta = fragmentManager.beginTransaction();
        
        Fragment prev = fragmentManager.findFragmentByTag(TAG);
        if (prev != null)
            ta.remove(prev);
        
        Bundle args = new Bundle();
        args.putSerializable(ARG_ITEM_ENTRY, item);
        TooltipFragment fragment = new TooltipFragment();
        fragment.setArguments(args);
        
        ta.addToBackStack(BACKSTACK_TOOLTIP);
        fragment.show(ta, TAG);
        
        return fragment;
    }
    
    private ItemEntry item;
    
    public InventoryActivity activity() {
        return (InventoryActivity) getActivity();
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        item = (ItemEntry) getArguments().getSerializable(ARG_ITEM_ENTRY);
        setStyle(DialogFragment.STYLE_NO_TITLE, 0);
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();
        item = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.inventory_fragment_tooltip, container, false);
        final ItemEntry item = this.item;
        
        TextView name = (TextView) v.findViewById(R.id.name);
        name.setText(item.getName());
        
        final LruCache<String, Bitmap> bitmapCache = activity().getCacheFragment().getBitmapCache();
        Bitmap bitmap = bitmapCache.get(item.getIcon());
        if (bitmap != null) {
            Drawable icon = new BitmapDrawable(getResources(), bitmap);
            icon.setBounds(0, 0, getResources().getDimensionPixelSize(R.dimen.item_icon_size), getResources().getDimensionPixelSize(R.dimen.item_icon_size));

            //name.setCompoundDrawablesWithIntrinsicBounds(null, null, icon, null);
            name.setCompoundDrawables(null, null, icon, null);
        } else {
            if (LoadIconTask.loadIconInto(name, item.getIcon(), getActivity())) {
                LoadIconTask.setCallback(name, new LoadIconTask.Callback() {
                    @Override
                    public void OnLoadComplete(View view, Bitmap bitmap) {
                        bitmapCache.put(item.getIcon(), bitmap);
                        
                        Drawable icon = new BitmapDrawable(getResources(), bitmap);
                        icon.setBounds(0, 0, getResources().getDimensionPixelSize(R.dimen.item_icon_size), getResources().getDimensionPixelSize(R.dimen.item_icon_size));
                        
                        //name.setCompoundDrawablesWithIntrinsicBounds(null, null, icon, null);
                        ((TextView) view).setCompoundDrawables(null, null, icon, null);
                    }
                });
            }
        }
        
        int color = getColor(item.getCategory());
        if (color != 0) {
            name.setTypeface(null, Typeface.BOLD);
            name.setTextColor(color);
        }
        
        ((TextView) v.findViewById(R.id.attack)).setText(String.valueOf(item.getAttack()));
        ((TextView) v.findViewById(R.id.defense)).setText(String.valueOf(item.getDefense()));
        ((TextView) v.findViewById(R.id.level)).setText(String.valueOf(item.getLevel()));
        
        SpannableStringBuilder ttbonus = new SpannableStringBuilder();
        List<Bonus> bonuses = item.getBonuses();
        for (int i = 0; i < bonuses.size(); i++) {
            String line = bonuses.get(i).getLabel();
            if (i < bonuses.size() - 1)
                line += "<br />";
            ttbonus.append(Html.fromHtml(line));
        }
        ((TextView) v.findViewById(R.id.bonus)).setText(ttbonus);
        
        final InventoryActivity act = activity();
        if (item.getType() == Type.INVENTORY || item.getType() == Type.TREASURE) {
            final Type originalType = item.getType();
            v.findViewById(R.id.actions).setVisibility(View.VISIBLE);
            if (item.getRestriction() != null) {
                TextView restriction = (TextView) v.findViewById(R.id.equip_error);
                restriction.setText(item.getRestriction());
                restriction.setVisibility(View.VISIBLE);
            } else {
                Button button = (Button) v.findViewById(R.id.equip);
                button.setVisibility(View.VISIBLE);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        activity().indicateLoading(true);
                        dismiss();
                        new AsyncTask<Void, Void, Boolean>() {
                            private boolean error = false;
                            @Override
                            protected Boolean doInBackground(Void... params) {
                                try {
                                    boolean ret = activity().userApi().getInventoryAPI().equipItem(item);
                                    if (ret) {
                                        item.changeType(Type.EQUIPMENT);
                                        TNTDBHelper dbh = act.container().db();
                                        synchronized (dbh) {
                                            dbh.update(item);
                                            dbh.close();
                                        }
                                    }
                                    return ret;
                                } catch (SessionTimeoutException e) {
                                    error = true;
                                } catch (ConnectionException e) {
                                    error = true;
                                }
                                return false;
                            }
                            @Override
                            protected void onPostExecute(Boolean result) {
                                act.indicateLoading(false);
                                if (result) {
                                    act.updateList(originalType, true);
                                    act.updateList(Type.EQUIPMENT, true);
                                } else if (error) {
                                    Toast.makeText(act, getString(R.string.error_connection_error), Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(act, getString(R.string.error_item_cannot_equip, item.getName()), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }.execute();
                    }
                });
            }
        } else if (item.getType() == Type.EQUIPMENT) {
            v.findViewById(R.id.actions).setVisibility(View.VISIBLE);
            Button button = (Button) v.findViewById(R.id.unequip);
            button.setVisibility(View.VISIBLE);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    activity().indicateLoading(true);
                    dismiss();
                    new AsyncTask<Void, Void, Boolean>() {
                        private boolean error = false;
                        @Override
                        protected Boolean doInBackground(Void... params) {
                            try {
                                boolean ret = activity().userApi().getInventoryAPI().unequipItem(item);
                                
                                Type targetType = Type.INVENTORY;
                                for (Bonus b : item.getBonuses()) {
                                    if (b.getLabel().contains("Seltenheit")) {
                                        targetType = Type.TREASURE;
                                        break;
                                    }
                                }
                                
                                if (ret) {
                                    item.changeType(targetType);
                                    TNTDBHelper dbh = act.container().db();
                                    synchronized (dbh) {
                                        dbh.update(item);
                                        dbh.close();
                                    }
                                }
                                return ret;
                            } catch (SessionTimeoutException e) {
                                error = true;
                            } catch (ConnectionException e) {
                                error = true;
                            }
                            return false;
                        }
                        @Override
                        protected void onPostExecute(Boolean result) {
                            act.indicateLoading(false);
                            if (result) {
                                act.updateList(Type.EQUIPMENT, true);
                                act.updateList(item.getType(), true);
                            } else if (error) {
                                Toast.makeText(act, getString(R.string.error_connection_error), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(act, getString(R.string.error_item_cannot_equip, item.getName()), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }.execute();
                }
            });
        }
        
        /*v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });*/
        
        return v;
    }
    
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        
        return dialog;
    }
    
    public int getColor(Inventory.Category cat) {
        switch (cat) {
            case NORMAL:
                break;
            case EPIC:
                return getResources().getColor(R.color.item_category_epic);
            case PREMIUM:
                return getResources().getColor(R.color.item_category_premium);
            case SPECIAL:
                return getResources().getColor(R.color.item_category_special);
            case RUNES:
                return getResources().getColor(R.color.item_category_runes);
            case UNIQUE:
                return getResources().getColor(R.color.item_category_unique);
            case RARE_UNIQUE:
                return getResources().getColor(R.color.item_category_rare_unique);
        }
        return 0;
    }
}
