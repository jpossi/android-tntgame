/*******************************************************************************
 * Copyright 2014 Jascha Starke <jascha@ja-s.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package tnt.game;

import tnt.game.api.Inventory;
import tnt.game.api.Inventory.Type;
import tnt.game.core.Activity;
import tnt.game.core.TabFragmentPagerAdapter;
import tnt.game.inventory.CacheFragment;
import tnt.game.inventory.ItemListFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public class InventoryActivity extends Activity {
    private TabFragmentPagerAdapter adapter;
    private ItemListFragment[] itemlists = new ItemListFragment[3];
    private MenuItem refreshAction;
    private boolean loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_inventory);

        if (user() == null) {
            Toast.makeText(this, getString(R.string.error_not_loggedin), Toast.LENGTH_SHORT).show();
            gotoLogin();
            finish();
            return;
        }
        
        // Show the Up button in the action bar.
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setIcon(container().state().getUserIcon(user()));
        
        adapter = new TabFragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public void onEntrySelected(int position) {
                ItemListFragment list = itemlists[position];
                if (list != null)
                    list.onPageSelect();
            }

            @Override
            public int getCount() {
                return itemlists.length;
            }
            
            @Override
            public ItemListFragment getItem(int position) {
                if (itemlists[position] == null) {
                    if (itemlists[position] == null) {
                        itemlists[position] = new ItemListFragment();
                        Bundle args = new Bundle();
                        args.putSerializable(ItemListFragment.ARG_INV_TYPE, Type.byValue(position));
                        itemlists[position].setArguments(args);
                    }
                }
                return itemlists[position];
            }
            
            @Override
            public String getPageTitle(int position) {
                switch (Inventory.Type.byValue(position)) {
                    case EQUIPMENT:
                        return getString(R.string.inventory_equipment);
                    case INVENTORY:
                        return getString(R.string.inventory_inventory);
                    case TREASURE:
                        return getString(R.string.inventory_treasure);
                }
                return super.getPageTitle(position).toString();
            }
            @Override
            protected int getDefaultPage() {
                return Inventory.Type.INVENTORY.value();
            }
        };
        
        getSupportActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        
        ViewPager pager = (ViewPager) findViewById(R.id.pager);
        adapter.registerWithActionBar(pager, getSupportActionBar());
        
        pager.post(new Runnable() {
            @Override
            public void run() {
                ItemListFragment fragment = itemlists[getSupportActionBar().getSelectedNavigationIndex()];
                if (fragment != null)
                    fragment.onPageSelect();
            }
        });
    }
    
    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        
        if (getSupportFragmentManager().getFragments() != null) {
            for (Fragment f : getSupportFragmentManager().getFragments()) {
                if (f instanceof ItemListFragment && f.getArguments() != null) {
                    int position = ((Type) f.getArguments().getSerializable(ItemListFragment.ARG_INV_TYPE)).value();
                    if (itemlists[position] == null) {
                        itemlists[position] = (ItemListFragment) f;
                    }
                }
            }
        }
    }

    public CacheFragment getCacheFragment() {
        return CacheFragment.findOrCreateRetainFragment(getSupportFragmentManager());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (refreshAction != null && refreshAction.getActionView() != null) {
            refreshAction.getActionView().clearAnimation();
            refreshAction.setActionView(null);
        }
        
        getSupportMenuInflater().inflate(R.menu.inventory, menu);
        
        refreshAction = menu.findItem(R.id.action_refresh);

        indicateLoading(loading);
        return true;
    }
    
    public void indicateLoading(boolean on) {
        // Before onCreate:setContentView
        //requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        
        //setSupportProgressBarIndeterminate(true);
        //setSupportProgressBarIndeterminateVisibility(false);
        loading = on;
        if (refreshAction != null) {
            if (on) {
                if (refreshAction.getActionView() == null) {
                    LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    ImageView iv = (ImageView) inflater.inflate(R.layout.action_refresh, null);
                    
                    Animation rotation = AnimationUtils.loadAnimation(this, R.anim.clockwise_refresh);
                    rotation.setRepeatCount(Animation.INFINITE);
                    iv.startAnimation(rotation);
                    
                    refreshAction.setActionView(iv);
                }
                refreshAction.setEnabled(false);
            } else if (!on) {
                if (refreshAction.getActionView() != null) {
                    refreshAction.getActionView().clearAnimation();
                    refreshAction.setActionView(null);
                }
                refreshAction.setEnabled(true);
            }
        }
    }
    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh:
                int index = getSupportActionBar().getSelectedNavigationIndex();
                updateList(Type.byValue(index), true);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void updateList(Type type, boolean fullUpdate) {
        ItemListFragment itemlist = itemlists[type.value()];
        if (itemlist != null)
            itemlist.update(fullUpdate);
    }
}
