/*******************************************************************************
 * Copyright 2014 Jascha Starke <jascha@ja-s.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package tnt.game.hunt;

import java.util.List;

import tnt.game.HuntActivity;
import tnt.game.R;
import tnt.game.api.Hunt.Opponent;
import tnt.game.core.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

public class StartFragment extends Fragment {
    protected List<Opponent> opponents = null;
    private SeekBar durBar;
    private TextView currentDuration;
    
    protected HuntActivity activity() {
        return ((HuntActivity) getActivity());
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.hunt_fragment_start, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        opponents = activity().api().getOpponents();
        
        String[] strings = new String[opponents.size()];
        for (int i = 0; i < opponents.size(); i++) {
            strings[i] =  opponents.get(i).getName();
        }
        
        final Spinner spinner = (Spinner) getView().findViewById(R.id.opponent);
        currentDuration = (TextView) getView().findViewById(R.id.current_duration);
        durBar = (SeekBar) getView().findViewById(R.id.duration);
        durBar.setMax(14);
        durBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}
            
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                currentDuration.setText(getHourString(getHours(progress)));
            }
        });
        currentDuration.setText(getHourString(getHours(durBar.getProgress())));
        
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, strings);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        
        ((Button) getView().findViewById(R.id.start_hunt)).setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setEnabled(false);
                Opponent o = opponents.get(spinner.getSelectedItemPosition());
                activity().startHunt(o, getHours(durBar.getProgress()));
            }
        });
    }
    protected String getHourString(int hours) {
        return getResources().getQuantityString(R.plurals.hours, hours, hours);
    }
    protected static int getHours(int progressPosition) {
        switch (progressPosition) {
            case 12:
                return 24;
            case 13:
                return 48;
            case 14:
                return 72;
            default:
                return progressPosition + 1;
        }
    }
}
