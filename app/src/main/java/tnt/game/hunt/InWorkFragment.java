/*******************************************************************************
 * Copyright 2014 Jascha Starke <jascha@ja-s.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package tnt.game.hunt;

import tnt.game.HuntActivity;
import tnt.game.R;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class InWorkFragment extends Fragment {
    private static final String KEY_TIMEEND = "TIMEEND";
    protected static final String TIME_FORMAT = "%02d:%02d:%02d";
    private CountDownTimer timer;
    private int timeend;
    
    protected HuntActivity activity() {
        return ((HuntActivity) getActivity());
    }
    
    private static int time() {
        return (int) (System.currentTimeMillis() / 1000);
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.hunt_fragment_inwork, container, false);
    }
    
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            timeend = savedInstanceState.getInt(KEY_TIMEEND);
        } else {
            timeend = time() + activity().api().getTimer();
        }
    }
    
    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(KEY_TIMEEND, timeend);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStart() {
        super.onStart();
        start();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (timer != null)
            timer.cancel();
        timer = null;
    }
    private void start() {
        if (timer != null)
            timer.cancel();
        
        Holder h = getHolder(null);
        int timeleft = timeend - time();
        showTimeLeft(h, timeleft);
        h.opponent.setText(activity().api().getCurrentOpponent());
        
        timer = new CountDownTimer(timeleft * 1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                //Log.d("tnt.game", "InWork: Tick: " + (millisUntilFinished / 1000));
                showTimeLeft(getHolder(null), (int) (millisUntilFinished / 1000));
            }
            
            @Override
            public void onFinish() {
                if (getActivity() != null)
                    activity().load();
            }
        }.start();
    }
    
    protected void showTimeLeft(Holder holder, int t) {
        if (holder != null) {
            int h = (int) Math.floor(t / 3600);
            int m = (int) Math.floor((t % 3600) / 60);
            int s = t % 60;
            holder.time_left.setText(String.format(TIME_FORMAT, h, m, s));
        }
    }
    
    private static class Holder {
        TextView time_left;
        TextView opponent;
    }
    private Holder getHolder(View v) {
        if (v == null)
            v = getView();
        if (v != null) {
            if (v.getTag() != null) {
                return (Holder) v.getTag();
            } else {
                Holder h = new Holder();
                h.time_left = (TextView) v.findViewById(R.id.time_left);
                h.opponent = (TextView) v.findViewById(R.id.opponent);
                v.setTag(h);
                return h;
            }
        }
        return null;
    }
}
