/*******************************************************************************
 * Copyright 2014 Jascha Starke <jascha@ja-s.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package tnt.game.hunt;

import tnt.game.R;
import tnt.game.api.Hunt;
import tnt.game.api.Hunt.Reward;
import tnt.game.core.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class RewardFragment extends DialogFragment {
    private int gold = -1;
    private int exp = -1;
    
    public RewardFragment loadRewards(Hunt api) {
        gold = api.getReward(Reward.GOLD);
        exp = api.getReward(Reward.EXP);
        return this;
    }

    public Activity activity() {
        return (Activity) getActivity();
    }
    
    public View onCreateDialogview(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.hunt_fragment_reward, container, false);
        Hunt api = activity().userApi().getHuntAPI();
        int g = gold > -1 ? gold : api.getReward(Reward.GOLD);
        int e = exp > -1 ? exp : api.getReward(Reward.EXP);
        
        ((TextView) v.findViewById(R.id.hunt_reward_gold)).setText(String.valueOf(g));
        ((TextView) v.findViewById(R.id.hunt_reward_exp)).setText(String.valueOf(e));
        return v;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new AlertDialog.Builder(getActivity())
            .setTitle(getString(R.string.hunt_reward))
            .setPositiveButton(android.R.string.ok,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        getDialog().cancel();
                    }
                }
            )
            .setView(onCreateDialogview(getActivity().getLayoutInflater(), null, savedInstanceState))
            .setCancelable(true)
            .create();
    }
}
