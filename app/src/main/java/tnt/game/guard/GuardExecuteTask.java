/*******************************************************************************
 * Copyright 2014 Jascha Starke <jascha@ja-s.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package tnt.game.guard;

import tnt.game.BattleReportActivity;
import tnt.game.GuardActivity;
import tnt.game.R;
import tnt.game.api.BattleReport;
import tnt.game.api.ConnectionException;
import tnt.game.api.Guard;
import tnt.game.api.Guard.Difficulty;
import tnt.game.api.SessionTimeoutException;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.util.Log;
import android.widget.Toast;

public class GuardExecuteTask extends AsyncTask<Integer, Integer, Integer> {
    private Guard api;
    private Difficulty difficulty;
    protected BattleReport report;
    private WakeLock wakeLock;
    private boolean sendToService = false;
    private int progress = 0;
    private int left = -1;
    private GuardActivity context;
    private ProgressDialog dialog;
    public int availCount = -1;
    
    public GuardExecuteTask(GuardActivity context, ProgressDialog dialog, Difficulty difficulty) {
        this.context = context;
        this.api = context.userApi().getGuardAPI();
        this.difficulty = difficulty;
        this.dialog = dialog;
        
        PowerManager powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "GuardExecuteTaskWakelock");
        wakeLock.acquire();
    }
    
    public void sendToService() {
        sendToService = true;
        
        GuardExecuteService.previewNotification(context, context.user(), getProgress(), getProgress() + left);
        
        cancel(false);
    }
    
    @Override
    protected Integer doInBackground(Integer... params) {
        context.user().setGuardDifficulty(difficulty.value());
        context.container().db().update(context.user());
        context.container().db().close();

        boolean all = false;
        int count = params[0];
        if (count == -1) {
            count = 1;
            all = true;
        }
        for (int i = Math.min(0, count); i < count; i++) {
            left = count - i;
            if (isCancelled())
                return left;
            try {
                if (count > 1) {
                    api.doSilentGuard(difficulty);
                    progress = i + 1;
                    publishProgress(i + 1);
                } else if (all) {
                    report = api.doAllGuard(difficulty);
                } else {
                    report = api.doGuard(difficulty);
                }
            } catch (SessionTimeoutException e) {
                Log.e("tnt.game", "Exception: Failed to execute TownDefense", e);
                return left;
            } catch (ConnectionException e) {
                Log.e("tnt.game", "Exception: Failed to execute TownDefense", e);
                return left;
            }
        }
        for (int waits = 0; waits < 5 && dialog == null; waits++) {
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {}
        }
        return left = 0;
    }
    
    public int getProgress() {
        return progress;
    }
    public int getMaximum() {
        return progress + left;
    }

    @Override
    protected void onPostExecute(Integer left) {
        if (dialog != null)
            dialog.dismiss();
        
        if (left > 0) {
            Toast.makeText(context, context.getString(R.string.error_connection_error), Toast.LENGTH_SHORT).show();
        }
        
        if (availCount > -1)
            context.updateAvailableCount(availCount - getProgress()); 
        if (report != null) {
            Intent intent = new Intent(context, BattleReportActivity.class);
            intent.putExtra(BattleReportActivity.EXTRA_REPORT, report);
            context.startActivity(intent);
        }
        context.update();
        
        wakeLock.release();
    }

    @Override
    protected void onCancelled() {
        if (dialog != null)
            dialog.dismiss();
        if (availCount > -1)
            context.updateAvailableCount(availCount - getProgress());
        if (sendToService && left > 0) {
            Intent service = new Intent(context, GuardExecuteService.class);
            service.putExtra(GuardExecuteService.EXTRA_USER, context.user());
            service.putExtra(GuardExecuteService.EXTRA_OFFSET, getProgress());
            service.putExtra(GuardExecuteService.EXTRA_REPEAT, left);
            service.putExtra(GuardExecuteService.EXTRA_DIFFICULTY, difficulty.value());
            context.startService(service);
        }
        if (!sendToService)
            context.update();
        wakeLock.release();
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        if (availCount > -1)
            context.updateAvailableCount(availCount - values[0]); 
        if (dialog != null)
            dialog.setProgress(values[0]);
        super.onProgressUpdate(values);
    }
    
    public void updateContext(GuardActivity newContext, ProgressDialog newDialog) {
        this.context = newContext;
        this.dialog = newDialog;
    }
}
