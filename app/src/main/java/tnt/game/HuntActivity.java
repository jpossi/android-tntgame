/*******************************************************************************
 * Copyright 2014 Jascha Starke <jascha@ja-s.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package tnt.game;

import com.actionbarsherlock.view.Menu;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.ViewStub;
import android.widget.TextView;
import android.widget.Toast;
import tnt.game.api.BattleReport;
import tnt.game.api.ConnectionException;
import tnt.game.api.Hunt;
import tnt.game.api.Hunt.Opponent;
import tnt.game.api.Hunt.State;
import tnt.game.api.SessionTimeoutException;
import tnt.game.core.Activity;
import tnt.game.hunt.InWorkFragment;
import tnt.game.hunt.RewardFragment;
import tnt.game.hunt.StartFragment;
import tnt.game.storage.ActivityEntry;
import tnt.game.storage.ActivityHelper;
import tnt.game.storage.ActivityEntry.Type;

public class HuntActivity extends Activity {
    private InWorkFragment inwork;
    private StartFragment start;
    private LoadingFragment loading;
    private Hunt api;
    private AsyncTask<Void, Void, State> task;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hunt);

        // Show the Up button in the action bar.
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setIcon(container().state().getUserIcon(user()));
        
        if (user() == null) {
            Toast.makeText(this, getString(R.string.error_not_loggedin), Toast.LENGTH_SHORT).show();
            gotoLogin();
            finish();
            return;
        }
        
        api = container().api().getUserAPI(user()).getHuntAPI();
        
        registerForCloseOnLogout();
        registerForNewMessage();
        
        if (savedInstanceState != null) {
            return;
        }
        load();
    }
    
    public boolean onSupportNavigateUp() {
        Intent intent = new Intent(this, StatusActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(EXTRA_USER_ENTRY, user());
        startActivity(intent);
        return true;
    }

    public Hunt api() {
        return api;
    }
    
    class HuntTask extends AsyncTask<Void, Void, State> {
        private boolean error = false;
        private ActivityEntry activityEntry;
        @Override
        protected State doInBackground(Void... params) {
            activityEntry = container().db().findActivity(user());
            if (activityEntry != null && !activityEntry.isOutdated()) {
                if (activityEntry.getType() == Type.BOSSBATTLE) {
                    container().db().close();
                    return State.IN_BOSSBATTLE;
                } else if (activityEntry.getType() == Type.MISSION) {
                    container().db().close();
                    return State.MISSION_ACTIVE;
                }
            }
            State result;
            try {
                result = api.fetchCurrentState();
            } catch (ConnectionException e) {
                error = true;
                container().db().close();
                return null;
            }
            if (result == State.IN_BOSSBATTLE) {
                int bosstimer = container().api().getUserAPI(user()).getBossAPI().getBossTimer();
                activityEntry = container().db().activityHelper(user()).bossFightToEndIn(activityEntry, bosstimer);
            } else if (result == State.IN_WORK) {
                activityEntry = container().db().activityHelper(user()).huntToEndIn(activityEntry, api().getTimer(), api().getCurrentOpponent());
            } else if (result == State.MISSION_ACTIVE) {
                int timer = container().api().getUserAPI(user()).getMissionsAPI().getTimer();
                activityEntry = container().db().activityHelper(user()).missionToEndIn(activityEntry, timer);
            } else if (activityEntry != null) {
                container().db().delete(activityEntry);
            }
            container().db().close();
            return result;
        }
        @Override
        protected void onPostExecute(State result) {
            task = null;
            if (error) {
                Toast.makeText(HuntActivity.this, getString(R.string.error_connection_error), Toast.LENGTH_SHORT).show();
                finish();
            } else if (result != null) {
                switch (result) {
                    case IN_WORK:
                        displayInWork();
                        break;
                    case IN_WORK_FINISHED:
                        new RewardFragment().loadRewards(userApi().getHuntAPI()).show(getSupportFragmentManager(), "HUNT_REWARD");
                        task = new HuntTask().execute();
                        break;
                    case MISSION_ACTIVE:
                        Toast.makeText(HuntActivity.this, getString(R.string.error_currently_on_mission), Toast.LENGTH_SHORT).show();
                        finish();
                        break;
                    case MISSION_FINISHED:
                        displayBattleReportNotification(userApi().getMissionsAPI().getBattleReport());
                        task = new HuntTask().execute();
                        break;
                    case IN_BOSSBATTLE:
                        Toast.makeText(HuntActivity.this, getString(R.string.error_bossbattle_active), Toast.LENGTH_SHORT).show();
                        finish();
                        break;
                    case OPPONENT_CHOICE:
                        if (start == null)
                            start = new StartFragment();
                        getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fragment_container, start)
                            .commit();
                        break;
                }
            }
        }
    }
    
    public void load() {
        if (loading == null)
            loading = new LoadingFragment();
        getSupportFragmentManager().beginTransaction()
            .replace(R.id.fragment_container, loading)
            .commit();
        
        if (task != null)
            task.cancel(false);
        task = new HuntTask().execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getSupportMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (task != null) {
            task.cancel(false);
            task = null;
        }
    }

    protected void displayInWork() {
        if (inwork == null)
            inwork = new InWorkFragment();
        if (findViewById(R.id.fragment_container) != null) {
            getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, inwork)
                .commit();
        }
    }
    
    public void startHunt(final Opponent opponent, final int duration) {
        new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... params) {
                try {
                     boolean result = api.startHunt(opponent, duration);
                     ActivityEntry entry = ActivityHelper.prepareHuntEntry(api.getTimer(), user());
                     entry.setName(api.getCurrentOpponent());
                     container().db().insert(entry);
                     return result;
                } catch (SessionTimeoutException e) {
                    Toast.makeText(HuntActivity.this, getString(R.string.session_error), Toast.LENGTH_SHORT).show();
                    finish();
                } catch (ConnectionException e) {
                    Toast.makeText(HuntActivity.this, getString(R.string.error_currently_on_mission), Toast.LENGTH_SHORT).show();
                    finish();
                }
                return false;
            }
            @Override
            protected void onPostExecute(Boolean result) {
                if (result) {
                    displayInWork();
                }
            }
        }.execute();
    }
    
    public void cancelHunt(View view) {
        if (view != null)
            view.setEnabled(false);
        new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... params) {
                try {
                    boolean ret = api.abortHunt();
                    container().db().deleteActivity(user());
                    container().db().close();
                    return ret;
                } catch (SessionTimeoutException e) {
                    Toast.makeText(HuntActivity.this, getString(R.string.session_error), Toast.LENGTH_SHORT).show();
                    finish();
                } catch (ConnectionException e) {
                    Toast.makeText(HuntActivity.this, getString(R.string.error_currently_on_mission), Toast.LENGTH_SHORT).show();
                    finish();
                }
                return false;
            }
            @Override
            protected void onPostExecute(Boolean result) {
                if (result) {
                    new RewardFragment().loadRewards(userApi().getHuntAPI()).show(getSupportFragmentManager(), "HUNT_REWARD");
                    task = new HuntTask().execute();
                }
            }
        }.execute();
    }
    
    private BattleReport currentReport;
    public void displayBattleReportNotification(BattleReport report) {
        currentReport = report;
        View notification = findViewById(R.id.notification);
        if (notification instanceof ViewStub)
            notification = ((ViewStub) notification).inflate();
        notification.setVisibility(View.VISIBLE);
        ((TextView) notification.findViewById(R.id.notification_text)).setText(getString(R.string.battlereport));
        ((TextView) notification.findViewById(R.id.notification_subtext)).setText(getString(R.string.battle_report_winner, currentReport.getWinnerName()));
        notification.findViewById(R.id.notification_dismiss).setVisibility(View.VISIBLE);
    }
    
    public void clickNotification(View v) {
        if (currentReport != null) {
            Intent intent = new Intent(this, BattleReportActivity.class);
            intent.putExtra(BattleReportActivity.EXTRA_REPORT, currentReport);
            startActivity(intent);
        }
        v.setVisibility(View.GONE);
        currentReport = null;
    }
    public void dismissNotification(View v) {
        findViewById(R.id.notification).setVisibility(View.GONE);
        currentReport = null;
    }
}
