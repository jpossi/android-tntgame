/*******************************************************************************
 * Copyright 2014 Jascha Starke <jascha@ja-s.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package tnt.game;

import android.os.Bundle;
import android.preference.PreferenceActivity;

public class PreferencesActivity extends PreferenceActivity {
    public static final String KEY_NOTIFICATION_ENABLED = "pref_key_notification_enabled";
    public static final String KEY_NOTIFICATION_VIBRATE = "pref_key_notification_vibrate";
    public static final String KEY_MENU_LAYOUT_CHOICE = "pref_group_menu_layout_choice";
    public static final String KEY_ALWAYS_ON = "pref_key_always_on_enabled";
    public static final String KEY_ALWAYS_ON_ONLY_WIFI = "pref_key_always_on_only_wifi";
    
    @Override
    @SuppressWarnings("deprecation")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
    }
}
