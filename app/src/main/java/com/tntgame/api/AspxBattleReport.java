/*******************************************************************************
 * Copyright 2014 Jascha Starke <jascha@ja-s.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.tntgame.api;

import java.util.Arrays;
import java.util.List;

import tnt.game.api.BattleReport;

import com.tntgame.api.fetcher.BattleReportParser;
import com.tntgame.api.fetcher.BattleReportParser.Attr;

public class AspxBattleReport implements BattleReport {
    private static final long serialVersionUID = 4905510157436666044L;
    
    private boolean won = false;
    private Winner winner;
    private String attacker;
    private String defender;
    private int gold;
    private int exp;
    private List<String> msgs;
    private String opponentImg;
    
    public AspxBattleReport(String me, BattleReportParser parser) {
        populateFromParser(parser);
    }
    
    protected void populateFromParser(BattleReportParser parser) {
        attacker = parser.get(Attr.ATTACKER);
        defender = parser.get(Attr.DEFENDER);
        winner = parser.get(Attr.WINNER).equals(attacker) ? Winner.ATTACKER : Winner.DEFENDER;
        opponentImg = parser.getOpponentImage();
        
        String v;
        v = parser.get(Attr.GOLD);
        gold = v != null ? Integer.valueOf(v) : 0;
        v = parser.get(Attr.EXP);
        exp = v != null ? Integer.valueOf(v) : 0;
        
        msgs = Arrays.asList(parser.getLogMsgs());
    }
    
    @Override
    public boolean isWon() {
        return won;
    }
    @Override
    public Winner getWinner() {
        return winner;
    }
    @Override
    public String getWinnerName() {
        return getWinner() == Winner.ATTACKER ? getAttacker() : getDefener();
    }
    @Override
    public int getGold() {
        return gold;
    }
    @Override
    public int getExp() {
        return exp;
    }

    @Override
    public String getAttacker() {
        return attacker;
    }
    @Override
    public String getDefener() {
        return defender;
    }

    @Override
    public List<String> getLog() {
        return msgs;
    }

    @Override
    public String getOpponentImage() {
        return opponentImg;
    }
}
