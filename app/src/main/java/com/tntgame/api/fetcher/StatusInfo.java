/*******************************************************************************
 * Copyright 2014 Jascha Starke <jascha@ja-s.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.tntgame.api.fetcher;

import java.io.IOException;
import java.net.MalformedURLException;

import org.jsoup.Connection;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import tnt.game.api.AuthenticationException;
import tnt.game.api.ConnectionException;
import tnt.game.api.SessionTimeoutException;
import android.util.Log;

public class StatusInfo extends AbstractFetcher {
    private static final String URI_STATUS_INFO = "http://" + HTTP_DOMAIN + "/Main/WebGame/StatusInfo.aspx";
    private static final int FETCH_LOGIN_INVALID = -1;
    
    public static enum ATTR implements Selection {
        NAME("#ctl00_ContentPlaceHolder1_CharName"),
        LEVEL("#ctl00_ContentPlaceHolder1_CharLevel"),
        NEXT_LEVEL("#ctl00_ContentPlaceHolder1_CharNextLevel"),
        
        GOLD("#ctl00_ContentPlaceHolder6_MailCheck1_lbGold"),
        RELICTS("#ctl00_ContentPlaceHolder6_MailCheck1_lbDia"),
        DIAS("#ctl00_ContentPlaceHolder6_MailCheck1_lbDiaPrem"),

        ENDURANCE("#ctl00_ContentPlaceHolder1_CharDuration"),
        RANK("#ctl00_ContentPlaceHolder1_CharRanking"),

        MAIN("[base=A]"),
        DEFENSE("[base=D]"),
        LIFE("[base=H]"),
        LUCK("[base=L]"),
        SKILL_POINTS("#ctl00_ContentPlaceHolder1_FreePoints"),

        PMAIN("[cost=A]"),
        PDEFENSE("[cost=D]"),
        PLIFE("[cost=H]"),
        PLUCK("[cost=L]");

        private String s;
        ATTR(String selector) {
            this.s = selector;
        }
        public String getSelector() {
            return s;
        }
    }
    
    private boolean isLogin;
    private Document document;

    public StatusInfo(GeneralFetcher connection) {
        super(connection);
    }
    
    public void setInitialLogin(boolean allowRetry) {
        this.isLogin = allowRetry;
    }
    
    public int fetch() throws ConnectionException {
        try {
            Response response;
            if (isLogin) {
                document = null;
                Connection con = Jsoup.connect(URI_STATUS_INFO)
                        .timeout(TIMEOUT)
                        .userAgent(USER_AGENT)
                        .cookie(USERLOGIN_COOKIE, connection.getLoginID())
                        .followRedirects(false);
                response = con.execute();
                if (response.statusCode() != 200) {
                    con.request().cookies().putAll(response.cookies());
                    response = con.execute();
                    if (con.response().statusCode() == 200)
                        document = response.parse();
                } else {
                    document = response.parse();
                }
                if (document != null) {
                    connection.updateSessionId(con.request().cookie(SESSION_ID_COOKIE));
                }
            } else {
                response = request(newConnection(URI_STATUS_INFO)
                    .followRedirects(false));
                document = response.parse();
            }
            
            isLogin = false;
            if (document == null)
                Log.w("tnt.game.StatusInfo.fetch", URI_STATUS_INFO + " - " + response.statusCode() + " " + response.statusMessage());
            
            updateGlobals(document);
            return document != null ? FETCH_SUCCESS : FETCH_LOGIN_INVALID;
        } catch (MalformedURLException e) {
            Log.e("tnt.game", "Exception: Failed to fetch Website", e);
            throw new ConnectionException(e);
        } catch (IOException e) {
            Log.e("tnt.game", "Exception: Failed to fetch Website", e);
            throw new ConnectionException(e);
        } catch (AuthenticationException e) {
            Log.e("tnt.game", "Exception: Failed to fetch Website", e);
            throw new ConnectionException(e);
        }
    }
    
    public boolean isDataAvailable() {
        return document != null;
    }
    
    public String get(ATTR attr) {
        return get(document, attr);
    }

    public String getUserImagePath() {
        if (document != null) {
            return document.select("#ctl00_ContentPlaceHolder2_Hero1_imgBase").get(0).absUrl("src");
            //return src.replaceAll("^\\.\\./", URI_MAIN_PATH);
        }
        return null;
    }
    public String getMainStat() {
        if (document != null) {
            Elements main = document.select(ATTR.MAIN.getSelector());
            if (main.size() > 0) {
                return getContent(main.get(0).parent().parent().child(0).child(0));
            }
        }
        return null;
    }
    
    public int clickIncrease(String button) throws ConnectionException, SessionTimeoutException {
        String x = button + ".x", y = button + ".y";

        try {
            Response response = validate(prepareForm(document.select("form").first())
                .data(x, "1")
                .data(y, "1")
                .execute());
            if (response.body().contains("Nicht genug Gold !")){
                return 0;
            }
        } catch (IOException e) {
            Log.e("tnt.game", "Exception: Failed to fetch Website", e);
            throw new ConnectionException(e);
        }
        return 1;
    }
}
