/*******************************************************************************
 * Copyright 2014 Jascha Starke <jascha@ja-s.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.tntgame.api.fetcher;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public abstract class AbstractParser {
    public interface Selection {
        public String getSelector();
    }
    
    protected static String get(Element node, String val) {
        if (node != null) {
            Elements els = node.select(val);
            if (els.size() < 1)
                return null;
            return getContent(els.get(0));
        }
        return null;
    }
    protected static String getHtml(Element node, String val) {
        if (node != null) {
            Elements els = node.select(val);
            if (els.size() < 1)
                return null;
            return getHTMLContent(els.get(0));
        }
        return null;
    }
    protected static String getContent(Element node) {
        if (node == null)
            return null;
        return node.text();
    }
    protected static String getHTMLContent(Element node) {
        if (node == null)
            return null;
        if (node.children().size() > 0) {
            if (node.child(0).nodeName().equalsIgnoreCase("font"))
                node = node.child(0);
        }
        return node.html();
    }
    protected static String get(Element node, Selection val) {
        return get(node, val.getSelector());
    }
}
