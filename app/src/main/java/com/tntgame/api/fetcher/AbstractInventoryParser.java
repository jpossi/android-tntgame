/*******************************************************************************
 * Copyright 2014 Jascha Starke <jascha@ja-s.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.tntgame.api.fetcher;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class AbstractInventoryParser extends AbstractFetcher {
    protected static final Pattern REGEX_TOOLTIP_MATCH =
        Pattern.compile("\\$create\\(Telerik\\.Web\\.UI\\.RadToolTip, \\{[^\\{]*\"targetControlID\":\"(\\w+)\"[^\\}]*\\}, null, null, \\$get\\(\"(\\w+)\"\\)\\);");
    
    public AbstractInventoryParser(GeneralFetcher connection) {
        super(connection);
    }

    protected static Map<String, String> getTooltipMap(String rawDocument) {
        Map<String, String> ret = new HashMap<String, String>();
        Matcher matcher = REGEX_TOOLTIP_MATCH.matcher(rawDocument);
        while (matcher.find()) {
            ret.put(matcher.group(1), matcher.group(2));
        }
        return ret;
    }
}
