/*******************************************************************************
 * Copyright 2014 Jascha Starke <jascha@ja-s.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.tntgame.api.fetcher;

import org.jsoup.nodes.Document;

import tnt.game.api.ConnectionException;

public class BossBattleFetcher extends AbstractFetcher {
    protected Document document;
    
    public BossBattleFetcher(GeneralFetcher connection) {
        super(connection);
    }
    
    public int getSecondsLeft() {
        if (document == null)
            return -1;
        String t = document.select("#c1").text();
        if (t == null || t.length() == 0) // sometimes it happens, that there is no time left...
            return 0;
        return Integer.parseInt(t);
    }

    @Override
    public int fetch() throws ConnectionException {
        throw new IllegalAccessError("Manually call not needed");
    }

    @Override
    public boolean isDataAvailable() {
        return document != null;
    }
}
