/*******************************************************************************
 * Copyright 2014 Jascha Starke <jascha@ja-s.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.tntgame.api.fetcher;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Connection;
import org.jsoup.Connection.Response;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import tnt.game.api.AuthenticationException;
import tnt.game.api.ConnectionException;
import tnt.game.api.SessionTimeoutException;
import android.util.Log;

public class MessageFetcher extends AbstractFetcher {
    private static final String URI_MESSAGES = "http://" + HTTP_DOMAIN + "/Main/WebGame/ReadMessage.aspx";
    private static final String URI_NEW_MESSAGE = "http://" + HTTP_DOMAIN + "/Main/WebGame/WriteMessage.aspx";
    private static final String URI_MESSAGE = "http://" + HTTP_DOMAIN + "/Main/WebGame/ShowMessage.aspx";
    private static final Pattern MID_REGEX = Pattern.compile("mID=(\\d+)$");
    private static final Pattern PID_REGEX = Pattern.compile("id=(\\d+)$");
    
    public MessageFetcher(GeneralFetcher connection) {
        super(connection);
    }
    
    private Document messages;
    private Document selectedMessage;
    private Document writeMessage;

    @Override
    public int fetch() throws ConnectionException {
        try {
            Connection con = newConnection(URI_MESSAGES)
                    .followRedirects(true);

            Response response = request(con);
            
            Document document = updateGlobals(response.parse());

            messages = document;
            
            return document != null ? FETCH_SUCCESS : FETCH_ERROR;
        } catch (MalformedURLException e) {
            Log.e("tnt.game", "Exception: Failed to fetch Website", e);
            throw new ConnectionException(e);
        } catch (IOException e) {
            Log.e("tnt.game", "Exception: Failed to fetch Website", e);
            throw new ConnectionException(e);
        } catch (AuthenticationException e) {
            Log.e("tnt.game", "Exception: Failed to fetch Website", e);
            throw new ConnectionException(e);
        }
    }

    public int deleteAll(int id) throws SessionTimeoutException, ConnectionException {
        if (messages == null)
            return FETCH_ERROR;
        
        try {
            Connection form = prepareForm(messages.select("form").first())
                    .data("ctl00$ContentPlaceHolder1$delButton.x", "1")
                    .data("ctl00$ContentPlaceHolder1$delButton.y", "1");
            for (MessageData message : getMessages(id)) {
                form.data("ctl00$ContentPlaceHolder1$MsgContainer$PanelPosteingang$delbox" + message.id, "On");
            }
            validate(form
                    .execute());
        } catch (IOException e) {
            Log.e("tnt.game", "Exception: Failed to fetch Website", e);
            throw new ConnectionException(e);
        }
        
        return FETCH_SUCCESS;
    }

    @Override
    public boolean isDataAvailable() {
        return messages != null;
    }
    
    public static class MessageData {
        public int id;
        public boolean bold;
        public String date;
        public String subject;
        public String senderName;
        public int senderId;
    }
    
    public List<MessageData> getMessages(int type) {
        String sel;
        switch (type) {
            case 0:
                sel = "#PanelPosteingang table";
                break;
            case 1:
                sel = "#PanelPostausgang table";
                break;
            case 2:
                sel = "#PanelInformation table";
                break;
            default:
                throw new IllegalArgumentException("Unknown Messages.TYPE " + type);
        }

        Element table = messages.select(sel).last();
        if (table == null)
            return new ArrayList<MessageData>(0);

        Elements rows = table.select("tr");
        List<MessageData> list = new ArrayList<MessageData>(rows.size());
        for (Element row : rows) {
            if (row.select("a").size() > 0) {
                Element a = row.select("a").get(1);
                Matcher m = MID_REGEX.matcher(a.attr("href"));
                if (m.find()) {
                    MessageData d = new MessageData();
                    d.id = Integer.parseInt(m.group(1));
                    d.date = getContent(row.select("td").get(1));
                    Element b = a.select("b").first();
                    if (b != null) {
                        d.bold = true;
                        d.subject = b.text();
                    } else {
                        d.bold = false;
                        d.subject = a.text();
                    }

                    Element sa = row.select("a").first();
                    d.senderName = getContent(sa);
                    Matcher ma = PID_REGEX.matcher(a.attr("href"));
                    if (m.find()) {
                        d.senderId = Integer.parseInt(ma.group(1));
                    }

                    list.add(d);
                }
            }
        }
        
        return list;
    }

    public int fetchMessage(int id) throws ConnectionException {
        try {
            Connection con = newConnection(URI_MESSAGE)
                    .followRedirects(true);
            
            con.data("mID", String.valueOf(id));
            
            Response response = request(con);
            
            selectedMessage = updateGlobals(response.parse());
            
            return selectedMessage != null ? FETCH_SUCCESS : FETCH_ERROR;
        } catch (MalformedURLException e) {
            Log.e("tnt.game", "Exception: Failed to fetch Website", e);
            throw new ConnectionException(e);
        } catch (IOException e) {
            Log.e("tnt.game", "Exception: Failed to fetch Website", e);
            throw new ConnectionException(e);
        } catch (AuthenticationException e) {
            Log.e("tnt.game", "Exception: Failed to fetch Website", e);
            throw new ConnectionException(e);
        }
    }
    public String get(MsgAttr attr) {
        if (selectedMessage == null)
            return null;
        return get(selectedMessage, attr);
    }
    public String getMsgContent() {
        if (selectedMessage == null)
            return null;
        return getHtml(selectedMessage, "#ctl00_ContentPlaceHolder1_lbMessage");
    }
    public enum MsgAttr implements Selection {
        FROM("#ctl00_ContentPlaceHolder1_lbFrom"),
        SUBJECT("#ctl00_ContentPlaceHolder1_lbSubjekt"),
        DATE("#ctl00_ContentPlaceHolder1_lbTime");

        private String sel;
        MsgAttr(String selector) {
            this.sel = selector;
        }
        @Override
        public String getSelector() {
            return sel;
        }
        
    }

    public int send(String recipient, String subject, String content) throws ConnectionException {
        try {
            if (writeMessage == null) {
                writeMessage = request(newConnection(URI_NEW_MESSAGE).followRedirects(false)).parse();
            }
            writeMessage.select("#ctl00_ContentPlaceHolder1_tbName").first().val(recipient);
            writeMessage.select("#ctl00_ContentPlaceHolder1_tbSubjekt").first().val(subject);
            writeMessage.select("#ctl00_ContentPlaceHolder1_tbmessage").first().val(content);
            
            Response response = request(prepareForm(writeMessage.select("form").first())
                    .data("ctl00$ContentPlaceHolder1$Button1.x", "1")
                    .data("ctl00$ContentPlaceHolder1$Button1.y", "1"));
            
            return response.body().contains("Nachricht gesendet") ? FETCH_SUCCESS : FETCH_ERROR;
        } catch (MalformedURLException e) {
            Log.e("tnt.game", "Exception: Failed to fetch Website", e);
            throw new ConnectionException(e);
        } catch (IOException e) {
            Log.e("tnt.game", "Exception: Failed to fetch Website", e);
            throw new ConnectionException(e);
        } catch (AuthenticationException e) {
            Log.e("tnt.game", "Exception: Failed to fetch Website", e);
            throw new ConnectionException(e);
        }
    }

    public int deleteMessage(int id) throws ConnectionException {
        if (selectedMessage == null) {
            int r = fetchMessage(id);
            if (r != FETCH_SUCCESS)
                return r;
        }
        Element form = selectedMessage.select("form").first();
        Matcher m = MID_REGEX.matcher(form.attr("action"));
        if (m.find()) {
            int mID = Integer.parseInt(m.group(1));
            if (mID != id) {
                int r = fetchMessage(id);
                if (r != FETCH_SUCCESS)
                    return r;
                form = selectedMessage.select("form").first();
            }
            
            try {
                validate(prepareForm(form)
                    .data("ctl00$ContentPlaceHolder1$Button1.x", "1")
                    .data("ctl00$ContentPlaceHolder1$Button1.y", "1")
                    .execute());
                return FETCH_SUCCESS;
            } catch (SessionTimeoutException e) {
                Log.e("tnt.game", "Exception: Failed to fetch Website", e);
                throw new ConnectionException(e);
            } catch (IOException e) {
                Log.e("tnt.game", "Exception: Failed to fetch Website", e);
                throw new ConnectionException(e);
            }
        }
        return FETCH_ERROR;
    }
}
