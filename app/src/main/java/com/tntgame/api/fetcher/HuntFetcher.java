/*******************************************************************************
 * Copyright 2014 Jascha Starke <jascha@ja-s.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.tntgame.api.fetcher;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Map;
import java.util.TreeMap;

import org.jsoup.Connection.Response;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import tnt.game.api.AuthenticationException;
import tnt.game.api.ConnectionException;
import tnt.game.api.SessionTimeoutException;
import android.util.Log;

public class HuntFetcher extends AbstractFetcher {
    private static final String URI_HUNT = "http://" + HTTP_DOMAIN + "/Main/WebGame/Jagd.aspx";
    protected Document document;
    protected boolean in_work = false;
    protected boolean in_work_finished = false;
    
    public HuntFetcher(GeneralFetcher connection) {
        super(connection);
    }

    public static final int BATTLE_REPORT = -4;
    public static final int IN_BOSSBATTLE = -3;
    public static final int MISSION_ACTIVE = -1;
    public static final int IN_WORK = 2;
    public static final int IN_WORK_FINISHED = 3;
    public int fetch() throws ConnectionException {
        document = null;
        in_work = false;
        in_work_finished = false;
        try {
            Response response = request(newConnection(URI_HUNT)
                    .followRedirects(true));
            
            if (response.url().toString().contains("InWork.aspx")) {
                if (response.body().contains("Beute von der Jagd")) {
                    document = response.parse();
                    updateGlobals(document);
                    in_work_finished = true;
                    return document != null ? IN_WORK_FINISHED : FETCH_ERROR;
                } else if (!response.body().contains("Du bist derzeit auf der Jagd")) {
                    connection.getMissions().document = updateGlobals(response.parse());
                    connection.getMissions().in_work = true;
                    return MISSION_ACTIVE;
                } else {
                    document = response.parse();
                    in_work = true;
                    updateGlobals(document);
                    return document != null ? IN_WORK : FETCH_ERROR;
                }
            } else if (response.url().toString().contains("InBossBattle.aspx")) {
                connection.getBossBattle().document = updateGlobals(response.parse());
                return IN_BOSSBATTLE;
            } else if (response.url().toString().contains("BattleReport.aspx")) {
                connection.getMissions().lastBattle = updateGlobals(response.parse());
                return BATTLE_REPORT;
            } else {
                document = updateGlobals(response.parse());
            }
            return document != null ? FETCH_SUCCESS : FETCH_ERROR;
        } catch (MalformedURLException e) {
            Log.e("tnt.game", "Exception: Failed to fetch Website", e);
            throw new ConnectionException(e);
        } catch (IOException e) {
            Log.e("tnt.game", "Exception: Failed to fetch Website", e);
            throw new ConnectionException(e);
        } catch (AuthenticationException e) {
            Log.e("tnt.game", "Exception: Failed to fetch Website", e);
            throw new ConnectionException(e);
        }
    }

    @Override
    public boolean isDataAvailable() {
        return document != null;
    }
    
    public boolean isInWork() {
        return in_work && isDataAvailable();
    }
    
    public int getSecondsLeft() {
        if (!isInWork())
            return -1;
        String t = document.select("#c1").text();
        if (t == null || t.length() == 0) // sometimes it happens, that there is no time left...
            return 0;
        return Integer.parseInt(t);
    }
    
    public String[] getSelectEntry(int idx) {
        Elements table = document.select("table#ContentPlaceHolder1_tblMain tr");
        Element row = table.get(idx + 1); // + header row
        Elements cols = row.select("td");
        return new String[]{cols.get(0).text(), cols.get(1).text()};
    }
    
    public enum SelectedMissionAttr implements Selection {
        TITLE("#ContentPlaceHolder1_lbName"),
        DETAIL("#ContentPlaceHolder1_lbDetails"),
        DURATION("#ContentPlaceHolder1_lbDuration"),
        ENDURANCE("#ContentPlaceHolder1_lbEndu"),
        REWARD("#ContentPlaceHolder1_lblReport");
        
        private String sel;
        SelectedMissionAttr(String sel) {
            this.sel = sel;
        }
        public String getSelector() {
            return sel;
        }
    }

    public void runHunt(int opponent, int hours) throws SessionTimeoutException, ConnectionException {
        try {
            document.select("#ctl00_ContentPlaceHolder1_ddEnemy").first().val(String.valueOf(opponent));
            document.select("#ctl00_ContentPlaceHolder1_ddDuration").first().val(String.valueOf(hours));
            
            Response response = validate(prepareForm(document.select("form").first())
                .data("ctl00$ContentPlaceHolder1$Button1.x", "1")
                .data("ctl00$ContentPlaceHolder1$Button1.y", "1")
                .execute());
            
            in_work = true;
            document = response.parse();
        } catch (IOException e) {
            Log.e("tnt.game", "Exception: Failed to fetch Website", e);
            throw new ConnectionException(e);
        }
    }

    public Map<Integer, String> getEnemies() {
        if (document == null || in_work)
            return null;
        Map<Integer, String> ret = new TreeMap<Integer, String>();
        for (Element el : document.select("#ctl00_ContentPlaceHolder1_ddEnemy").first().children()) {
            ret.put(Integer.valueOf(el.attr("value")), el.text());
        }
        return ret;
    }

    public boolean cancelHunt() throws SessionTimeoutException, ConnectionException {
        if (!in_work || in_work_finished)
            return false;
        
        try {
            Response response = validate(prepareForm(document.select("form").first())
                .followRedirects(true)
                .data("ctl00$ContentPlaceHolder1$btnCancel.x", "1")
                .data("ctl00$ContentPlaceHolder1$btnCancel.y", "1")
                .execute());
            
            document = response.parse();
            in_work_finished = true;
        } catch (IOException e) {
            Log.e("tnt.game", "Exception: Failed to fetch Website", e);
            throw new ConnectionException(e);
        }
        
        return true;
    }
    
    public String getRewards() {
        if (!in_work_finished)
            return null;
        return get(document, "#ctl00_ContentPlaceHolder1_lbReport");
    }

    public String getOpponent() {
        if (!in_work)
            return null;
        return get(document, "#ctl00_ContentPlaceHolder1_lbType b");
    }
}
