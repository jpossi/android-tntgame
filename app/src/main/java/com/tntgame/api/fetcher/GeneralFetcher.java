/*******************************************************************************
 * Copyright 2014 Jascha Starke <jascha@ja-s.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.tntgame.api.fetcher;

import java.util.Date;

import org.jsoup.nodes.Document;

import com.tntgame.api.AspxUser;

public class GeneralFetcher {
    private AspxUser aspxUser;
    public GeneralFetcher(AspxUser aspxUser) {
        this.aspxUser = aspxUser;
    }
    public String getSessionId() {
        return aspxUser.getSession().getSessionId();
    }
    public void updateSessionId(String session) {
        aspxUser.getSession().id = session;
        updateSessionId();
    }
    public void updateSessionId() {
        aspxUser.getSession().ts = new Date();
    }
    public String getLoginID() {
        return aspxUser.getLoginID();
    }
    
    private BossBattleFetcher bossbattle;
    private HuntFetcher hunt;
    private MissionsFetcher missions;
    private StatusInfo status;
    private TownDefenseFetcher towndefense;
    private MessageFetcher message;
    private InventoryFetcher inventory;
    
    public BossBattleFetcher getBossBattle() {
        if (bossbattle == null)
            bossbattle = new BossBattleFetcher(this);
        return bossbattle;
    }
    public HuntFetcher getHunt() {
        if (hunt == null)
            hunt = new HuntFetcher(this);
        return hunt;
    }
    public MissionsFetcher getMissions() {
        if (missions == null)
            missions = new MissionsFetcher(this);
        return missions;
    }
    public StatusInfo getStatus() {
        if (status == null)
            status = new StatusInfo(this);
        return status;
    }
    public TownDefenseFetcher getTownDefense() {
        if (towndefense == null)
            towndefense = new TownDefenseFetcher(this);
        return towndefense;
    }
    public MessageFetcher getMessages() {
        if (message == null)
            message = new MessageFetcher(this);
        return message;
    }
    public InventoryFetcher getInventory() {
        if (inventory == null)
            inventory = new InventoryFetcher(this);
        return inventory;
    }
    
    public boolean globalsSet = false;
    protected void updateGlobals(Document lastDocument) {
        boolean newMessageAvailabeState = lastDocument.select("#ctl00_ContentPlaceHolder5_Hero2_lbMail").size() > 0;
        if (newMessageAvailable != newMessageAvailabeState) {
            newMessageAvailable = newMessageAvailabeState;
            if (globalsSet)
                aspxUser.notifyAboutNotificationUpdate();
        }
        globalsSet = true;
    }
    
    private boolean newMessageAvailable = false;
    
    public boolean isNewMessageAvailable() {
        return newMessageAvailable;
    }
}
