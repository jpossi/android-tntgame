/*******************************************************************************
 * Copyright 2014 Jascha Starke <jascha@ja-s.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.tntgame.api;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.tntgame.api.fetcher.BattleReportParser;
import com.tntgame.api.fetcher.MissionsFetcher;

import tnt.game.api.BattleReport;
import tnt.game.api.ConnectionException;
import tnt.game.api.Missions;
import tnt.game.api.SessionTimeoutException;

public class AspxMissions implements Missions {
    private AspxUser userApi;
    private MissionsFetcher fetcher;
    
    public AspxMissions(AspxUser userApi) {
        this.userApi = userApi;
        fetcher = userApi.connection.getMissions();
    }
    
    @Override
    public State fetchCurrentState() throws ConnectionException {
        int state = fetcher.fetch();
        switch (state) {
            case MissionsFetcher.HUNT_ACTIVE:
                return State.HUNT_ACTIVE;
            case MissionsFetcher.HUNT_FINISHED:
                return State.HUNT_FINISHED;
            case MissionsFetcher.IN_WORK:
                return State.IN_WORK;
            case MissionsFetcher.BATTLE_REPORT:
                return State.BATTLE_REPORT;
            case MissionsFetcher.IN_BOSSBATTLE:
                return State.IN_BOSSBATTLE;
            case MissionsFetcher.FETCH_SUCCESS:
                return State.MISSION_CHOICE;
            case MissionsFetcher.FETCH_ERROR:
                throw new ConnectionException("Document couldn't be parsed");
            default:
                throw new IllegalStateException("Invalid Fetcher return: " + state);
        }
    }
    
    @Override
    public Integer getTimer() {
        if (!fetcher.isInWork())
            return null;
        return fetcher.getSecondsLeft();
    }
    
    @Override
    public List<MissionHeader> getMissions() {
        if (!fetcher.isDataAvailable() || fetcher.isInWork()) {
            return null;
        }
        List<MissionHeader> list = new ArrayList<MissionHeader>();
        String[] entry;
        for (int i = 0; (entry = fetcher.getSelectEntry(i)) != null; i++) {
            list.add(new AspxMissionHeader(i, entry[0], entry[1]));
        }
        return list;
    }
    
    static class AspxMissionHeader implements MissionHeader {
        private static final long serialVersionUID = 6593989871040678606L;
        
        private String title;
        private String desc;
        private int position;

        public AspxMissionHeader(int position, String title, String desc) {
            this.position = position;
            this.title = title;
            this.desc = desc;
        }

        public int getPosition() {
            return position;
        }
        public String getTitle() {
            return title;
        }
        public String getDesc() {
            return desc;
        }
    }
    
    static class AspxMission implements MissionDetail {
        private static final long serialVersionUID = 3923826684367714182L;
        
        private int endurance;
        private String duration;
        private int exp;
        private int gold;
        private String desc;
        private String title;
        public int getEndurance() {
            return endurance;
        }
        public String getDuration() {
            return duration;
        }
        public int getExp() {
            return exp;
        }
        public int getGold() {
            return gold;
        }
        public String getDesc() {
            return desc;
        }
        public String getTitle() {
            return title;
        }
    }

    @Override
    public MissionDetail getMission(MissionHeader item) throws ConnectionException {
        AspxMissionHeader _item = (AspxMissionHeader) item;
        fetcher.fetchSelectMission(_item.getPosition());
        AspxMission mission = new AspxMission();
        mission.desc = fetcher.getSelectedMissionAttr(MissionsFetcher.SelectedMissionAttr.DETAIL);
        mission.title = fetcher.getSelectedMissionAttr(MissionsFetcher.SelectedMissionAttr.TITLE);
        mission.duration = fetcher.getSelectedMissionAttr(MissionsFetcher.SelectedMissionAttr.DURATION);
        String v;
        v = fetcher.getSelectedMissionAttr(MissionsFetcher.SelectedMissionAttr.ENDURANCE);
        mission.endurance = Integer.parseInt(v);
        v = fetcher.getSelectedMissionAttr(MissionsFetcher.SelectedMissionAttr.REWARD);
        Matcher m = Pattern.compile("Exp: (\\d+).*Gold: (\\d+)").matcher(v);
        if (m.find()) {
            mission.exp = Integer.parseInt(m.group(1));
            mission.gold = Integer.parseInt(m.group(2));
        }
        return mission;
    }

    /**
     * @param mission ignored
     */
    @Override
    public boolean startMission(MissionDetail mission) throws SessionTimeoutException, ConnectionException {
        fetcher.runSelectedMission();
        return fetcher.isInWork();
    }

    @Override
    public BattleReport getBattleReport() {
        BattleReportParser battle = fetcher.getBattle();
        return battle != null ? new AspxBattleReport(userApi.user.getName(), battle) : null;
    }
}
