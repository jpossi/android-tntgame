/*******************************************************************************
 * Copyright 2014 Jascha Starke <jascha@ja-s.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.tntgame.api;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import tnt.game.api.ConnectionException;
import tnt.game.api.Messages;
import tnt.game.api.SessionTimeoutException;
import android.util.Log;

import com.tntgame.api.fetcher.MessageFetcher;
import com.tntgame.api.fetcher.MessageFetcher.MessageData;
import com.tntgame.api.fetcher.MessageFetcher.MsgAttr;

public class AspxMessages implements Messages {
    private static final SimpleDateFormat DATE_PARSER = new SimpleDateFormat("dd.MM.yy HH:mm");
    private MessageFetcher fetcher;

    public AspxMessages(AspxUser aspxUser) {
        this.fetcher = aspxUser.connection.getMessages();
    }
    
    static class AspxMessage implements Message {
        private static final long serialVersionUID = -226077133886527862L;
        
        private int id;
        private String subject;
        private boolean unread = false;
        private Date date = null;
        private String contact;
        private String content;

        @Override
        public int getMessageId() {
            return id;
        }
        @Override
        public String getSubject() {
            return subject;
        }
        @Override
        public boolean isUnread() {
            return unread;
        }
        @Override
        public Date getDate() {
            return date;
        }
        @Override
        public String getContact() {
            return contact;
        }

        @Override
        public String getContent() {
            return content;
        }
        
    }

    @Override
    public boolean fetchBoxes() throws ConnectionException {
        if (fetcher.fetch() != MessageFetcher.FETCH_SUCCESS)
            throw new ConnectionException("Failed to parse messagelist");
        return true;
    }
    @Override
    public List<MessageHeader> getBox(BoxType type) {
        List<MessageData> messages = fetcher.getMessages(type.ordinal());

        List<MessageHeader> ret = new ArrayList<MessageHeader>(messages.size());
        for (MessageData msg : messages) {
            AspxMessage m = new AspxMessage();
            m.id = msg.id;
            m.unread = msg.bold;
            m.subject = msg.subject;
            try {
                m.date = DATE_PARSER.parse(msg.date);
            } catch (ParseException e) {
                Log.e("tnt.game", "Exception: Failed to parse Date: " + msg.date, e);
            }
            m.contact = msg.senderName;
            ret.add(m);
        }
        return ret;
    }

    @Override
    public boolean deleteAll(BoxType type) throws SessionTimeoutException, ConnectionException {
        int ret = fetcher.deleteAll(type == BoxType.INBOX ? 0 : 1);
        return ret == MessageFetcher.FETCH_SUCCESS;
    }

    @Override
    public Message readMessage(MessageHeader selectedMessage) throws ConnectionException {
        AspxMessage msg;
        if (selectedMessage instanceof AspxMessage) {
            msg = (AspxMessage) selectedMessage;
        } else {
            msg = new AspxMessage();
            msg.subject = selectedMessage.getSubject();
            msg.id = selectedMessage.getMessageId();
            msg.date = selectedMessage.getDate();
            msg.unread = selectedMessage.isUnread();
        }
        if (fetcher.fetchMessage(msg.getMessageId()) != MessageFetcher.FETCH_SUCCESS)
            throw new ConnectionException("Failed to parse message");
        
        int year = Calendar.getInstance().get(Calendar.YEAR);
        SimpleDateFormat source_format = new SimpleDateFormat("yyyy dd.MM. HH:mm:ss", Locale.GERMANY);
        String d = fetcher.get(MsgAttr.DATE);
        if (d == null || d.startsWith("[Kein Datum] ")) {
            msg.date = null;
        } else {
            try {
                msg.date = source_format.parse(year + " " + d);
                if (new Date().before(msg.date))
                    msg.date = source_format.parse((year - 1) + " " + d);
            } catch (ParseException e) {
                Log.e("tnt.game", "Exception: Failed to parse Date: " + d, e);
            }
        }
        msg.contact = fetcher.get(MsgAttr.FROM);
        msg.content = fetcher.getMsgContent();
        return msg;
    }

    @Override
    public boolean deleteMessage(Message msg) throws ConnectionException {
        int ret = fetcher.deleteMessage(msg.getMessageId());
        return ret == MessageFetcher.FETCH_SUCCESS;
    }

    @Override
    public boolean sendMessage(NewMessage msg) throws ConnectionException {
        int ret = fetcher.send(msg.getRecipient(), msg.getSubject(), msg.getContent());
        return ret == MessageFetcher.FETCH_SUCCESS;
    }
}
