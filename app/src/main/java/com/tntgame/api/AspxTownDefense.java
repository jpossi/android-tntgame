/*******************************************************************************
 * Copyright 2014 Jascha Starke <jascha@ja-s.de>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.tntgame.api;

import com.tntgame.api.fetcher.BattleReportParser;
import com.tntgame.api.fetcher.TownDefenseFetcher;

import tnt.game.api.BattleReport;
import tnt.game.api.ConnectionException;
import tnt.game.api.Guard;
import tnt.game.api.SessionTimeoutException;

public class AspxTownDefense implements Guard {
    private TownDefenseFetcher fetcher;
    private AspxUser userApi;
    private final Object currentlyFetchingLock = new Object();
    
    public AspxTownDefense(AspxUser aspxUser) {
        this.userApi = aspxUser;
        this.fetcher = aspxUser.connection.getTownDefense();
    }

    @Override
    public int getFreeCount() throws ConnectionException {
        synchronized (currentlyFetchingLock) {
            fetcher.fetch();
        }
        String c = fetcher.getCount();
        if (c == null)
            return 0;
        String[] x = c.split(" ");
        int count = 0;
        try {
            count = Integer.parseInt(x[0]);
        } catch (NumberFormatException e) {
            throw new ConnectionException("Failed to parse Guard-Count");
        }
        return count;
    }

    @Override
    public int getRegenTimeleft() {
        if (!fetcher.isDataAvailable())
            return -1;
        return fetcher.getSecondsLeft();
    }

    @Override
    public BattleReport doGuard(Difficulty difficulty) throws SessionTimeoutException, ConnectionException {
        BattleReportParser report;
        synchronized (currentlyFetchingLock) { // don't try, while currently fetching
            report = fetcher.start(difficulty);
        }
        if (report != null)
            return new AspxBattleReport(userApi.user.getName(), report);
        return null;
    }

    @Override
    public BattleReport doAllGuard(Difficulty difficulty) throws SessionTimeoutException, ConnectionException {
        BattleReportParser report;
        synchronized (currentlyFetchingLock) { // don't try, while currently fetching
            report = fetcher.start(difficulty, true);
        }
        if (report != null)
            return new AspxBattleReport(userApi.user.getName(), report);
        return null;
    }

    @Override
    public void doSilentGuard(Difficulty difficulty) throws ConnectionException, SessionTimeoutException {
        synchronized (currentlyFetchingLock) { // don't try, while currently fetching
            fetcher.startNoReport(difficulty);
        }
    }

}
